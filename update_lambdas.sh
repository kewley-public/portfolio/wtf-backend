#!/bin/bash

# Example arguments (one or more existing lambda names): WheresTheFundsBackend-WTFEventLambda60C6C961-JdEe9xaWDKOa
# You will first want to run `./gradlew clean buildLambdaZip" beforehand

declare -a FUNCTIONS=("$@")

BUILD_ZIP=./build/distributions/wtf-backend.zip

echo Build Zip Determined $BUILD_ZIP

for function in "${FUNCTIONS[@]}"; do
  echo Updating lambda: $function
  aws lambda update-function-code --function-name "$function" --zip-file "fileb://$BUILD_ZIP" > /dev/null
  echo Finished updating lambda: $function
done