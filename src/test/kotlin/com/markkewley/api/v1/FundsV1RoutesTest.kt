package com.markkewley.api.v1

import com.markkewley.BaseApiTest
import com.markkewley.models.Fund
import com.markkewley.serializers.CORE_JSON
import io.mockk.every
import kotlinx.serialization.encodeToString
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Status
import org.http4k.kotest.shouldHaveBody
import org.http4k.kotest.shouldHaveHeader
import org.http4k.kotest.shouldHaveStatus
import org.junit.jupiter.api.Test

val testFunds =
    listOf(
        Fund(
            id = "1",
            description = "Pets",
            amount = 200.0,
            categoryId = "1",
        ),
        Fund(
            id = "2",
            description = "Auto",
            amount = 100.0,
            categoryId = "2",
        ),
    )

class FundsV1RoutesTest : BaseApiTest() {
    @Test
    fun `Get Funds`() {
        val request = Request(Method.GET, "/api/v1/funds").header("Authorization", "Bearer $jwt")

        every { usersServiceMock.getUserByEmail(email = user.email) } returns user

        every { fundsServiceMock.getAllFundsForUser(userId = user.id) } returns testFunds

        val response = appWithMocks(request)

        response shouldHaveStatus Status.OK
        response shouldHaveHeader "Content-Type"
        response shouldHaveBody CORE_JSON.encodeToString<List<Fund>>(testFunds)
    }
}
