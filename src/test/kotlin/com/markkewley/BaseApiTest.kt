package com.markkewley

import com.markkewley.api.ApiRoutes
import com.markkewley.auth.models.User
import com.markkewley.auth.services.AuthService
import com.markkewley.auth.services.TokenService
import com.markkewley.services.CategoriesService
import com.markkewley.services.ExpenseEventsService
import com.markkewley.services.ExpensesService
import com.markkewley.services.FundsService
import com.markkewley.services.UsersService
import io.mockk.mockk
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext
import java.time.Instant

open class BaseApiTest() :
    BeforeAllCallback, AfterAllCallback {
    val user =
        User(
            id = "test",
            created = Instant.now(),
            email = "test@test.com",
            verified = true,
            roles = listOf(),
            userName = "Test User",
            passwordHash = "1234",
            googleOAuthId = "123456789",
            refreshToken = TokenService.generateRefreshToken("test"),
        )
    val jwt = TokenService.generateAccessToken(user = user, expiresAt = Instant.MAX)
    val usersServiceMock = mockk<UsersService>()
    val authServiceMock = AuthService(usersService = usersServiceMock)
    val expensesServiceMock = mockk<ExpensesService>()
    val categoriesServiceMock = mockk<CategoriesService>()
    val fundsServiceMock = mockk<FundsService>()
    val expenseEventsServiceMock = mockk<ExpenseEventsService>()

    val appWithMocks =
        ApiRoutes.routes(
            authService = authServiceMock,
            expensesService = expensesServiceMock,
            categoriesService = categoriesServiceMock,
            fundsService = fundsServiceMock,
            expenseEventsService = expenseEventsServiceMock,
            usersService = usersServiceMock,
        )

    override fun beforeAll(p0: ExtensionContext?) {
        System.setProperty("RUNNING_TESTS", "true")
    }

    override fun afterAll(p0: ExtensionContext?) {
        System.setProperty("RUNNING_TESTS", "false")
    }
}
