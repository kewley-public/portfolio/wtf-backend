package com.markkewley

import com.amazonaws.services.lambda.runtime.events.SQSEvent
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.lang.reflect.Proxy

@ExtendWith(DotenvExtension::class)
class WtfFnHandlerTest {
    @Test
    fun `event function test`() {
        WheresTheFundsFnHandler(
            SQSEvent().apply {
                records =
                    listOf(
                        SQSEvent.SQSMessage().apply {
                            eventSourceArn = "invalid"
                        },
                    )
            },
            proxy(),
        )
    }
}

// this is here in place of a mock
inline fun <reified T> proxy(): T = Proxy.newProxyInstance(T::class.java.classLoader, arrayOf(T::class.java)) { _, _, _ -> TODO("") } as T
