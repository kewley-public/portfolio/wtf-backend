package com.markkewley

import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext

class DotenvExtension : BeforeAllCallback, AfterAllCallback {
    override fun beforeAll(p0: ExtensionContext?) {
        System.setProperty("RUNNING_TESTS", "true")
    }

    override fun afterAll(p0: ExtensionContext?) {
        System.setProperty("RUNNING_TESTS", "false")
    }
}
