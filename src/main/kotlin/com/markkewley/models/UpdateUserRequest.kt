package com.markkewley.models

import kotlinx.serialization.Serializable

@Serializable
data class UpdateUserRequest(
    val userName: String? = null,
    val image: String? = null,
)
