package com.markkewley.models

import com.markkewley.serializers.InstantSerializer
import kotlinx.serialization.Serializable
import java.time.Instant

@Serializable
data class CreateExpenseEventRequest(
    val description: String,
    val amount: Double,
    val categoryId: String,
    @Serializable(InstantSerializer::class)
    val date: Instant,
    // TODO: Utilize rfc5545
    val recurrenceRule: String? = null,
)
