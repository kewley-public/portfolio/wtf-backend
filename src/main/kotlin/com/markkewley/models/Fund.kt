package com.markkewley.models

import kotlinx.serialization.Serializable

/**
 * Funds are constrained to months
 */
@Serializable
data class Fund(
    val id: String,
    val description: String,
    val amount: Double,
    val categoryId: String,
)
