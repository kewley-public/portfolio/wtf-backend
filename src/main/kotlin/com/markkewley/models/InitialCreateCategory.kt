package com.markkewley.models

data class InitialCreateCategory(
    val itemId: String,
    val description: String,
    val fundId: String,
)
