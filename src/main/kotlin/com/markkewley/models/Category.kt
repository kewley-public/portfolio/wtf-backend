package com.markkewley.models

import kotlinx.serialization.Serializable

@Serializable
data class Category(
    val id: String,
    val description: String,
    val colorCodeCategory: ColorCodeCategory,
    val fundId: String? = null,
)
