package com.markkewley.models

import kotlinx.serialization.Serializable

@Serializable
data class PaginatedResults<T>(
    val items: List<T>,
    val lastEvaluatedKey: Map<String, String>,
)
