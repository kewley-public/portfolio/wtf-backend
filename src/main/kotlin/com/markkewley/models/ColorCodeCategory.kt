package com.markkewley.models

enum class ColorCodeCategory {
    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE,
}
