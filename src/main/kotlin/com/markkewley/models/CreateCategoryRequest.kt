package com.markkewley.models

import kotlinx.serialization.Serializable

@Serializable
data class CreateCategoryRequest(
    val name: String,
    val colorCodeCategory: ColorCodeCategory,
    val fundId: String? = null,
)
