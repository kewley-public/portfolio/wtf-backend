package com.markkewley.models

import kotlinx.serialization.Serializable

@Serializable
data class CreateFundRequest(
    val amount: Double,
    val description: String,
    val categoryId: String,
)
