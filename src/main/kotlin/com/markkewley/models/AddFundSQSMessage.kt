package com.markkewley.models

import kotlinx.serialization.Serializable

@Serializable
data class AddFundSQSMessage(
    val userId: String,
    val fundId: String,
    val categoryId: String,
)
