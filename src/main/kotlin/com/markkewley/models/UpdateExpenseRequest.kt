package com.markkewley.models

import com.markkewley.serializers.InstantSerializer
import kotlinx.serialization.Serializable
import java.time.Instant

@Serializable
data class UpdateExpenseRequest(
    val amount: Double,
    val description: String,
    val categoryId: String,
    @Serializable(with = InstantSerializer::class)
    val date: Instant,
)
