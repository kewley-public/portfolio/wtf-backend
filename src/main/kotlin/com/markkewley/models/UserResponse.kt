package com.markkewley.models

import com.markkewley.auth.models.BaseUser
import com.markkewley.auth.models.Role
import com.markkewley.auth.models.User
import com.markkewley.serializers.InstantSerializer
import kotlinx.serialization.Serializable
import java.time.Instant

/**
 * What we wish to send to the client
 */
@Serializable
data class UserResponse(
    override val id: String,
    override val email: String,
    override val verified: Boolean,
    @Serializable(with = InstantSerializer::class)
    override val created: Instant,
    val roles: List<Role> = listOf(),
    val userName: String? = null,
) : BaseUser

fun fromUser(user: User) =
    UserResponse(
        id = user.id,
        email = user.email,
        verified = user.verified,
        roles = user.roles,
        userName = user.userName,
        created = user.created,
    )
