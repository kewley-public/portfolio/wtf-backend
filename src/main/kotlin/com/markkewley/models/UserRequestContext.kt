package com.markkewley.models

data class UserRequestContext(
    val userId: String,
    val roles: List<String>,
)
