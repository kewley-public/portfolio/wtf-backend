package com.markkewley.api.v1

import com.markkewley.models.CreateExpenseRequest
import com.markkewley.models.Expense
import com.markkewley.models.PaginatedResults
import com.markkewley.models.UpdateExpenseRequest
import com.markkewley.models.UserRequestContext
import com.markkewley.services.ExpensesService
import org.http4k.core.Body
import org.http4k.core.Method
import org.http4k.core.Method.GET
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.core.with
import org.http4k.format.KotlinxSerialization.auto
import org.http4k.lens.Path
import org.http4k.lens.RequestContextLens
import org.http4k.lens.string
import org.http4k.routing.RoutingHttpHandler
import org.http4k.routing.bind
import org.http4k.routing.routes

object ExpensesV1Routes {
    private val expensePaginatedLens = Body.auto<PaginatedResults<Expense>>().toLens()
    private val createExpenseLens = Body.auto<CreateExpenseRequest>().toLens()
    private val updateExpenseLens = Body.auto<UpdateExpenseRequest>().toLens()
    private val expenseLens = Body.auto<Expense>().toLens()

    fun routes(
        key: RequestContextLens<UserRequestContext>,
        expensesService: ExpensesService,
    ): RoutingHttpHandler =
        "/expenses" bind
            routes(
                "/" bind GET to { request ->
                    val context = key(request)
                    val lastEvaluatedKey = request.query("lastEvaluatedKey")
                    val start = request.query("start")
                    val end = request.query("end")
                    val descending = (request.query("descending")?.lowercase() ?: "true").toBooleanStrict()
                    val expenses =
                        expensesService.getAllExpensesForUser(
                            userId = context.userId,
                            descending = descending,
                            lastEvaluatedKey = lastEvaluatedKey,
                            start = start,
                            end = end,
                        )
                    Response(Status.OK).with(expensePaginatedLens of expenses).header("Content-Type", "application/json")
                },
                "/{id}" bind GET to { request ->
                    val idLens = Path.string().of("id")
                    val expenseId = idLens(request)
                    val context = key(request)
                    val expense = expensesService.getExpenseById(userId = context.userId, expenseId = expenseId)
                    Response(Status.OK).with(expenseLens of expense).header("Content-Type", "application/json")
                },
                "/" bind Method.POST to { request ->
                    val expenseRequest = createExpenseLens(request)
                    val context = key(request)
                    val expense = expensesService.createExpense(createExpenseRequest = expenseRequest, userId = context.userId)
                    Response(Status.CREATED).with(expenseLens of expense).header("Content-Type", "application/json")
                },
                "/{id}" bind Method.PUT to { request ->
                    val idLens = Path.string().of("id")
                    val expenseId = idLens(request)
                    val context = key(request)

                    val updateExpenseRequest = updateExpenseLens(request)
                    val updatedId =
                        expensesService.updateExpense(
                            userId = context.userId,
                            expenseId = expenseId,
                            updateExpenseRequest = updateExpenseRequest,
                        )
                    Response(Status.NO_CONTENT).body(updatedId)
                },
                "/{id}" bind Method.DELETE to { request ->
                    val idLens = Path.string().of("id")
                    val expenseId = idLens(request)
                    val context = key(request)

                    if (expensesService.deleteExpense(userId = context.userId, expenseId = expenseId)) {
                        Response(Status.NO_CONTENT)
                    } else {
                        Response(Status.NOT_FOUND).body("Unable to delete expense")
                    }
                },
            )
}
