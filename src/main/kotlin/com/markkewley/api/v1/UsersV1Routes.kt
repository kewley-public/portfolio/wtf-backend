package com.markkewley.api.v1

import com.markkewley.exception.NotFoundException
import com.markkewley.models.UpdateUserRequest
import com.markkewley.models.UserRequestContext
import com.markkewley.models.UserResponse
import com.markkewley.services.UsersService
import org.http4k.core.Body
import org.http4k.core.Method.GET
import org.http4k.core.Method.PUT
import org.http4k.core.Response
import org.http4k.core.Status.Companion.NO_CONTENT
import org.http4k.core.Status.Companion.OK
import org.http4k.core.with
import org.http4k.format.KotlinxSerialization.auto
import org.http4k.lens.RequestContextLens
import org.http4k.routing.RoutingHttpHandler
import org.http4k.routing.bind
import org.http4k.routing.routes

object UsersV1Routes {
    private val userLens = Body.auto<UserResponse>().toLens()
    private val updateUserLens = Body.auto<UpdateUserRequest>().toLens()

    fun routes(
        key: RequestContextLens<UserRequestContext>,
        usersService: UsersService,
    ): RoutingHttpHandler =
        "/users" bind
            routes(
                "/me" bind GET to { request ->
                    val context = key(request)
                    val myUser = usersService.getUserById(id = context.userId) ?: throw NotFoundException("User not found")
                    Response(OK).with(userLens of myUser).header("Content-Type", "application/json")
                },
                "/me" bind PUT to { request ->
                    val context = key(request)
                    val updateRequest = updateUserLens(request)
                    Response(NO_CONTENT)
                },
            )
}
