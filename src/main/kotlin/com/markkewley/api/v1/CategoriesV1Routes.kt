package com.markkewley.api.v1

import com.markkewley.exception.NotFoundException
import com.markkewley.models.Category
import com.markkewley.models.CreateCategoryRequest
import com.markkewley.models.UserRequestContext
import com.markkewley.services.CategoriesService
import org.http4k.core.Body
import org.http4k.core.Method.DELETE
import org.http4k.core.Method.GET
import org.http4k.core.Method.POST
import org.http4k.core.Response
import org.http4k.core.Status.Companion.CREATED
import org.http4k.core.Status.Companion.INTERNAL_SERVER_ERROR
import org.http4k.core.Status.Companion.NO_CONTENT
import org.http4k.core.Status.Companion.OK
import org.http4k.core.with
import org.http4k.format.KotlinxSerialization.auto
import org.http4k.lens.Path
import org.http4k.lens.RequestContextLens
import org.http4k.lens.string
import org.http4k.routing.RoutingHttpHandler
import org.http4k.routing.bind
import org.http4k.routing.routes

object CategoriesV1Routes {
    private val createCategoryLens = Body.auto<CreateCategoryRequest>().toLens()
    private val categoryLens = Body.auto<Category>().toLens()
    private val categoriesLens = Body.auto<List<Category>>().toLens()

    fun routes(
        key: RequestContextLens<UserRequestContext>,
        categoriesService: CategoriesService,
    ): RoutingHttpHandler =
        "/categories" bind
            routes(
                "/" bind POST to { request ->
                    val categoryRequest = createCategoryLens(request)
                    val context = key(request)
                    val category =
                        categoriesService.createCategory(createCategoryRequest = categoryRequest, userId = context.userId)
                    Response(CREATED).with(categoryLens of category).header("Content-Type", "application/json")
                },
                "/" bind GET to { request ->
                    val context = key(request)
                    val categories = categoriesService.getAllCategoriesForUser(userId = context.userId)
                    Response(OK).with(categoriesLens of categories).header("Content-Type", "application/json")
                },
                "/{id}" bind GET to { request ->
                    val context = key(request)
                    val idLens = Path.string().of("id")
                    val categoryId = idLens(request)
                    val category =
                        categoriesService.getCategoryById(
                            userId = context.userId,
                            categoryId = categoryId,
                        ) ?: throw NotFoundException()
                    Response(OK).with(categoryLens of category).header("Content-Type", "application/json")
                },
                "/{id}" bind DELETE to { request ->
                    val context = key(request)
                    val idLens = Path.string().of("id")
                    val categoryId = idLens(request)
                    if (categoriesService.deleteCategory(userId = context.userId, categoryId = categoryId)) {
                        Response(NO_CONTENT)
                    } else {
                        Response(INTERNAL_SERVER_ERROR)
                    }
                },
            )
}
