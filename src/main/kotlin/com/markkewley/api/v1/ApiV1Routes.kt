package com.markkewley.api.v1

import com.markkewley.models.UserRequestContext
import com.markkewley.services.CategoriesService
import com.markkewley.services.ExpenseEventsService
import com.markkewley.services.ExpensesService
import com.markkewley.services.FundsService
import com.markkewley.services.UsersService
import org.http4k.lens.RequestContextLens
import org.http4k.routing.RoutingHttpHandler
import org.http4k.routing.bind
import org.http4k.routing.routes

object ApiV1Routes {
    fun routes(
        key: RequestContextLens<UserRequestContext>,
        expensesService: ExpensesService,
        categoriesService: CategoriesService,
        fundsService: FundsService,
        expenseEventsService: ExpenseEventsService,
        usersService: UsersService,
    ): RoutingHttpHandler =
        "/v1" bind
            routes(
                ExpensesV1Routes.routes(key = key, expensesService = expensesService),
                CategoriesV1Routes.routes(key = key, categoriesService = categoriesService),
                FundsV1Routes.routes(key = key, expensesService = expensesService, fundsService = fundsService),
                EventsV1Routes.routes(key = key, expenseEventsService = expenseEventsService),
                UsersV1Routes.routes(key = key, usersService = usersService),
            )
}
