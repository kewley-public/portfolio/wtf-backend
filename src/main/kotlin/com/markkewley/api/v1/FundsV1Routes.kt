package com.markkewley.api.v1

import com.markkewley.exception.InvalidRequestException
import com.markkewley.models.CreateFundRequest
import com.markkewley.models.Expense
import com.markkewley.models.Fund
import com.markkewley.models.PaginatedResults
import com.markkewley.models.UserRequestContext
import com.markkewley.services.ExpensesService
import com.markkewley.services.FundsService
import org.http4k.core.Body
import org.http4k.core.Method.DELETE
import org.http4k.core.Method.GET
import org.http4k.core.Method.POST
import org.http4k.core.Response
import org.http4k.core.Status.Companion.CREATED
import org.http4k.core.Status.Companion.INTERNAL_SERVER_ERROR
import org.http4k.core.Status.Companion.NO_CONTENT
import org.http4k.core.Status.Companion.OK
import org.http4k.core.with
import org.http4k.format.KotlinxSerialization.auto
import org.http4k.lens.Path
import org.http4k.lens.RequestContextLens
import org.http4k.lens.string
import org.http4k.routing.RoutingHttpHandler
import org.http4k.routing.bind
import org.http4k.routing.routes

object FundsV1Routes {
    private val expensePaginatedLens = Body.auto<PaginatedResults<Expense>>().toLens()
    private val createFundLens = Body.auto<CreateFundRequest>().toLens()
    private val fundLens = Body.auto<Fund>().toLens()
    private val fundsLens = Body.auto<List<Fund>>().toLens()

    fun routes(
        key: RequestContextLens<UserRequestContext>,
        expensesService: ExpensesService,
        fundsService: FundsService,
    ): RoutingHttpHandler =
        "/funds" bind
            routes(
                "/" bind POST to { request ->
                    val fundsRequest = createFundLens(request)
                    val context = key(request)
                    val fund =
                        fundsService.createFund(createFundRequest = fundsRequest, userId = context.userId)
                    Response(CREATED).with(fundLens of fund).header("Content-Type", "application/json")
                },
                "/" bind GET to { request ->
                    val context = key(request)
                    val funds = fundsService.getAllFundsForUser(userId = context.userId)
                    Response(OK).with(fundsLens of funds).header("Content-Type", "application/json")
                },
                "/{id}/expenses" bind GET to { request ->
                    val idLens = Path.string().of("id")
                    val fundId = idLens(request)
                    val start = request.query("start") ?: throw InvalidRequestException("start parameter required")
                    val end = request.query("end") ?: throw InvalidRequestException("end parameter required")
                    val lastEvaluatedKey = request.query("lastEvaluatedKey")
                    val expenses =
                        expensesService.getAllExpensesForFund(
                            fundId = fundId,
                            start = start,
                            end = end,
                            lastEvaluatedKey = lastEvaluatedKey,
                        )
                    Response(OK).with(expensePaginatedLens of expenses).header("Content-Type", "application/json")
                },
                "/{id}" bind DELETE to { request ->
                    val context = key(request)
                    val idLens = Path.string().of("id")
                    val fundId = idLens(request)
                    if (fundsService.deleteFund(userId = context.userId, fundId = fundId)) {
                        Response(NO_CONTENT)
                    } else {
                        Response(INTERNAL_SERVER_ERROR).body("Unable to delete fund")
                    }
                },
            )
}
