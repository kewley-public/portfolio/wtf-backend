package com.markkewley.api.v1

import com.markkewley.exception.InvalidRequestException
import com.markkewley.models.CreateExpenseEventRequest
import com.markkewley.models.ExpenseEvent
import com.markkewley.models.PaginatedResults
import com.markkewley.models.UserRequestContext
import com.markkewley.services.ExpenseEventsService
import org.http4k.core.Body
import org.http4k.core.Method.GET
import org.http4k.core.Method.POST
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import org.http4k.core.with
import org.http4k.format.KotlinxSerialization.auto
import org.http4k.lens.RequestContextLens
import org.http4k.routing.RoutingHttpHandler
import org.http4k.routing.bind
import org.http4k.routing.routes

object EventsV1Routes {
    private val eventPaginatedList = Body.auto<PaginatedResults<ExpenseEvent>>().toLens()
    private val createEventRequest = Body.auto<CreateExpenseEventRequest>().toLens()
    private val expenseEvent = Body.auto<ExpenseEvent>().toLens()

    fun routes(
        key: RequestContextLens<UserRequestContext>,
        expenseEventsService: ExpenseEventsService,
    ): RoutingHttpHandler =
        "/events" bind
            routes(
                "/" bind GET to { request ->
                    val context = key(request)
                    val start = request.query("start") ?: throw InvalidRequestException("start parameter required")
                    val end = request.query("end") ?: throw InvalidRequestException("end parameter required")
                    val descending = (request.query("descending")?.lowercase() ?: "true").toBooleanStrict()
                    val lastEvaluatedKey = request.query("lastEvaluatedKey")
                    val expenseEvents =
                        expenseEventsService.getAllEventsForUser(
                            userId = context.userId,
                            start = start,
                            end = end,
                            descending = descending,
                            lastEvaluatedKey = lastEvaluatedKey,
                        )
                    Response(OK).with(eventPaginatedList of expenseEvents).header("Content-Type", "application/json")
                },
                "/" bind POST to { request ->
                    val context = key(request)
                    val createExpenseEventRequest = createEventRequest(request)
                    val createdExpense =
                        expenseEventsService.createEvent(
                            userId = context.userId,
                            createExpenseEventRequest = createExpenseEventRequest,
                        )
                    Response(OK).with(expenseEvent of createdExpense).header("Content-Type", "application/json")
                },
            )
}
