package com.markkewley.api

import com.markkewley.api.v1.ApiV1Routes
import com.markkewley.auth.services.AuthService
import com.markkewley.models.UserRequestContext
import com.markkewley.services.CategoriesService
import com.markkewley.services.ExpenseEventsService
import com.markkewley.services.ExpensesService
import com.markkewley.services.FundsService
import com.markkewley.services.UsersService
import org.http4k.core.RequestContexts
import org.http4k.core.then
import org.http4k.filter.ServerFilters
import org.http4k.lens.RequestContextKey
import org.http4k.routing.RoutingHttpHandler
import org.http4k.routing.bind
import org.http4k.routing.routes

object ApiRoutes {
    fun routes(
        authService: AuthService,
        expensesService: ExpensesService,
        categoriesService: CategoriesService,
        fundsService: FundsService,
        expenseEventsService: ExpenseEventsService,
        usersService: UsersService,
    ): RoutingHttpHandler {
        val contexts = RequestContexts()
        val key = RequestContextKey.required<UserRequestContext>(contexts)

        return ServerFilters.InitialiseRequestContext(contexts)
            .then(
                "/api" bind
                    routes(
                        ApiV1Routes.routes(
                            key = key,
                            expensesService = expensesService,
                            categoriesService = categoriesService,
                            fundsService = fundsService,
                            expenseEventsService = expenseEventsService,
                            usersService = usersService,
                        )
                            .withFilter(authService.requireAuthFilter(key)),
                    ),
            )
    }
}
