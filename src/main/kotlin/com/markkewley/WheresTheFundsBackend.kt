package com.markkewley

import com.amazonaws.services.lambda.runtime.ClientContext
import com.amazonaws.services.lambda.runtime.CognitoIdentity
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.LambdaLogger
import com.amazonaws.services.lambda.runtime.events.SQSEvent
import com.markkewley.admin.AdminApiRoutes
import com.markkewley.api.ApiRoutes
import com.markkewley.api.AuthApiRoutes
import com.markkewley.auth.services.AuthService
import com.markkewley.aws.Dynamo
import com.markkewley.exception.Http4kExceptionFilter
import com.markkewley.models.AddFundSQSMessage
import com.markkewley.serializers.CORE_JSON
import com.markkewley.services.CategoriesService
import com.markkewley.services.ExpenseEventsService
import com.markkewley.services.ExpensesService
import com.markkewley.services.FundsService
import com.markkewley.services.UsersService
import io.github.resilience4j.ratelimiter.RateLimiter
import io.github.resilience4j.ratelimiter.RateLimiterConfig
import io.github.resilience4j.ratelimiter.RequestNotPermitted
import kotlinx.serialization.encodeToString
import org.http4k.core.HttpHandler
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status.Companion.TOO_MANY_REQUESTS
import org.http4k.core.then
import org.http4k.filter.DebuggingFilters.PrintRequest
import org.http4k.filter.ResilienceFilters
import org.http4k.filter.ServerFilters
import org.http4k.format.AwsLambdaMoshi
import org.http4k.routing.routes
import org.http4k.server.Jetty
import org.http4k.server.asServer
import org.http4k.serverless.ApiGatewayRestLambdaFunction
import org.http4k.serverless.AppLoader
import org.http4k.serverless.AwsLambdaEventFunction
import org.http4k.serverless.FnLoader
import java.io.ByteArrayOutputStream
import java.time.Duration

object WheresTheFundsAppLoader : AppLoader {
    override fun invoke(env: Map<String, String>): HttpHandler {
        val categoriesService = CategoriesService(dynamoDbClient = Dynamo.dynamoDbClient)
        val fundsService = FundsService(dynamoDbClient = Dynamo.dynamoDbClient, categoriesService = categoriesService)
        val usersService =
            UsersService(dynamoDbClient = Dynamo.dynamoDbClient, categoriesService = categoriesService, fundsService = fundsService)
        val authService = AuthService(usersService = usersService)

        val expensesService =
            ExpensesService(
                dynamoDbClient = Dynamo.dynamoDbClient,
                categoriesService = categoriesService,
                fundsService = fundsService,
            )
        val expenseEventsService = ExpenseEventsService(dynamoDbClient = Dynamo.dynamoDbClient, categoriesService = categoriesService)

        val serverRoutes =
            PrintRequest()
                .then(
                    ServerFilters.CatchLensFailure.then(
                        routes(
                            AdminApiRoutes.routes(),
                            AuthApiRoutes.routes(authService = authService, usersService = usersService),
                            ApiRoutes.routes(
                                authService = authService,
                                expensesService = expensesService,
                                fundsService = fundsService,
                                categoriesService = categoriesService,
                                expenseEventsService = expenseEventsService,
                                usersService = usersService,
                            ),
                        ),
                    ),
                ).withFilter(Http4kExceptionFilter.exceptionHandlingFilter)

        if (WheresTheFundsConfig.enableCors) {
            serverRoutes.withFilter(CorsFilter.create())
        }

        // -------------------------------------------------------------------------------------------
        // RATE LIMITING - THIS IS GLOBAL AND NOT PER USER
        // -------------------------------------------------------------------------------------------

        // configure the rate limiter filter here
        val config =
            RateLimiterConfig.custom()
                .limitRefreshPeriod(Duration.ofSeconds(WheresTheFundsConfig.rateLimitRefreshPeriodInSeconds.toLong()))
                .limitForPeriod(WheresTheFundsConfig.rateLimitRequestLimit.toInt())
                .timeoutDuration(Duration.ofMillis(WheresTheFundsConfig.rateLimitTimeoutInMillis.toLong())).build()

        // set up the responses to sleep for a bit
        val rateLimits = ResilienceFilters.RateLimit(RateLimiter.of("ratelimiter", config))

        val rateLimitedRoutes = rateLimits.then(serverRoutes)

        // Handle Rate Limit Exceeding
        val rateLimitHandledRoutes = { request: Request ->
            try {
                rateLimitedRoutes(request)
            } catch (e: RequestNotPermitted) {
                // Return a 429 Too Many Requests response
                Response(TOO_MANY_REQUESTS).body("Rate limit exceeded")
            }
        }

        // -------------------------------------------------------------------------------------------

        return rateLimitHandledRoutes
    }
}

@Suppress("unused")
class WheresTheFundsAppFunction : ApiGatewayRestLambdaFunction(WheresTheFundsAppLoader)

@Suppress("unused")
class WheresTheFundsEventFunction : AwsLambdaEventFunction(
    FnLoader {
        WheresTheFundsFnHandler
    },
)

fun main(args: Array<String>) {
    val runServer = args.getOrNull(0)?.toBoolean() ?: true

    class MockContext : Context {
        override fun getAwsRequestId(): String {
            TODO("Not yet implemented")
        }

        override fun getLogGroupName(): String {
            TODO("Not yet implemented")
        }

        override fun getLogStreamName(): String {
            TODO("Not yet implemented")
        }

        override fun getFunctionName(): String {
            TODO("Not yet implemented")
        }

        override fun getFunctionVersion(): String {
            TODO("Not yet implemented")
        }

        override fun getInvokedFunctionArn(): String {
            TODO("Not yet implemented")
        }

        override fun getIdentity(): CognitoIdentity {
            TODO("Not yet implemented")
        }

        override fun getClientContext(): ClientContext {
            TODO("Not yet implemented")
        }

        override fun getRemainingTimeInMillis(): Int {
            TODO("Not yet implemented")
        }

        override fun getMemoryLimitInMB(): Int {
            TODO("Not yet implemented")
        }

        override fun getLogger(): LambdaLogger {
            TODO("Not yet implemented")
        }
    }

    fun runLambdaLocally() {
        val appLoader = WheresTheFundsAppLoader(System.getenv())
        val port = 9000

        appLoader.asServer(Jetty(port)).start()
    }

    fun submitSqSLocally(
        userId: String,
        fundId: String,
        categoryId: String,
    ) {
        val sqsEvent =
            SQSEvent().apply {
                records =
                    listOf(
                        SQSEvent.SQSMessage().apply {
                            eventSourceArn = WheresTheFundsConfig.fundSqsQueueArn
                            body =
                                CORE_JSON.encodeToString(
                                    AddFundSQSMessage(
                                        userId = userId,
                                        fundId = fundId,
                                        categoryId = categoryId,
                                    ),
                                )
                        },
                    )
            }

        val out = ByteArrayOutputStream()

        WheresTheFundsEventFunction().handleRequest(AwsLambdaMoshi.asInputStream(sqsEvent), out, MockContext())
    }

    if (runServer) {
        runLambdaLocally()
    } else {
        val userId = args.getOrNull(1) ?: throw IllegalArgumentException("userId expected when not running server")
        val fundId = args.getOrNull(2) ?: throw IllegalArgumentException("fundId expected when not running server")
        val categoryId =
            args.getOrNull(3) ?: throw IllegalArgumentException("categoryId expected when not running server")
        submitSqSLocally(userId = userId, fundId = fundId, categoryId = categoryId)
    }
}
