package com.markkewley.auth.api.v1

import com.markkewley.auth.exception.AuthenticationException
import com.markkewley.auth.models.AuthorizeUserRequest
import com.markkewley.auth.models.AuthorizedUserResponse
import com.markkewley.auth.models.RegisterAuthStrategyRequest
import com.markkewley.auth.models.RegisterUserRequest
import com.markkewley.auth.models.VerifyIdTokenRequest
import com.markkewley.auth.services.AuthService
import com.markkewley.auth.services.TokenService
import com.markkewley.models.UserResponse
import com.markkewley.models.fromUser
import com.markkewley.serializers.CORE_JSON
import com.markkewley.services.UsersService
import kotlinx.serialization.encodeToString
import org.http4k.core.Body
import org.http4k.core.Method.GET
import org.http4k.core.Method.POST
import org.http4k.core.Response
import org.http4k.core.Status.Companion.BAD_REQUEST
import org.http4k.core.Status.Companion.CREATED
import org.http4k.core.Status.Companion.INTERNAL_SERVER_ERROR
import org.http4k.core.Status.Companion.NO_CONTENT
import org.http4k.core.Status.Companion.OK
import org.http4k.core.cookie.cookie
import org.http4k.format.AwsLambdaMoshi.auto
import org.http4k.routing.RoutingHttpHandler
import org.http4k.routing.bind
import org.http4k.routing.routes

object AuthApiV1Routes {
    private val registerAuthStrategyRequestLens = Body.auto<RegisterAuthStrategyRequest>().toLens()

    fun routes(
        usersService: UsersService,
        authService: AuthService,
    ): RoutingHttpHandler =
        "/v1" bind
            routes(
                "/register-user" bind POST to { request ->
                    val registerUserRequest = CORE_JSON.decodeFromString<RegisterUserRequest>(request.bodyString())
                    val registerUserResult = usersService.registerUser(registerUserRequest = registerUserRequest)
                    Response(CREATED).body(CORE_JSON.encodeToString<UserResponse>(fromUser(registerUserResult)))
                        .header("Content-Type", "application/json")
                },
                "/verify-id-token" bind POST to { request ->
                    val verifyIdTokenRequest = CORE_JSON.decodeFromString<VerifyIdTokenRequest>(request.bodyString())
                    val (authorizeUserResponse, refreshToken) = authService.verifyIdToken(verifyIdTokenRequest = verifyIdTokenRequest)
                    Response(CREATED).body(CORE_JSON.encodeToString<AuthorizedUserResponse>(authorizeUserResponse))
                        .header("Content-Type", "application/json")
                        .cookie(TokenService.generateRefreshTokenCookie(refreshToken = refreshToken))
                },
                "/authorize-user" bind POST to { request ->
                    val authorizeUserRequest = CORE_JSON.decodeFromString<AuthorizeUserRequest>(request.bodyString())
                    val (authorizeUserResponse, refreshToken) =
                        authService.verifyEmailAndPassword(
                            authorizeUserRequest = authorizeUserRequest,
                        )

                    Response(CREATED).body(CORE_JSON.encodeToString<AuthorizedUserResponse>(authorizeUserResponse))
                        .header("Content-Type", "application/json")
                        .cookie(TokenService.generateRefreshTokenCookie(refreshToken = refreshToken))
                },
                "/register-auth-strategy" bind POST to { request ->
                    val registerAuthStrategy = registerAuthStrategyRequestLens(request)
                    usersService.registerAuthStrategy(registerAuthStrategyRequest = registerAuthStrategy)
                    Response(NO_CONTENT)
                },
                "/verify-user" bind POST to { request ->
                    val verificationToken = request.query("verificationToken")
                    if (verificationToken != null && usersService.verifyUser(verificationToken = verificationToken)) {
                        val user = usersService.getUserByVerificationToken(verificationToken)
                        if (user != null) {
                            Response(CREATED).body(CORE_JSON.encodeToString<UserResponse>(user))
                                .header("Content-Type", "application/json")
                        } else {
                            Response(INTERNAL_SERVER_ERROR)
                        }
                    } else if (verificationToken == null) {
                        Response(BAD_REQUEST).body("Verification Token Required")
                    } else {
                        Response(BAD_REQUEST).body("Invalid token")
                    }
                },
                "/refresh-session" bind GET to { request ->
                    val refreshToken =
                        TokenService.getRefreshTokenCookieFromRequest(request)
                            ?: throw AuthenticationException("No Refresh Token Found")
                    val newSession = authService.updateSession(refreshToken = refreshToken)
                    Response(OK).body(CORE_JSON.encodeToString<AuthorizedUserResponse>(newSession))
                },
            )
}
