package com.markkewley.api

import com.markkewley.auth.api.v1.AuthApiV1Routes
import com.markkewley.auth.services.AuthService
import com.markkewley.services.UsersService
import org.http4k.routing.RoutingHttpHandler
import org.http4k.routing.bind
import org.http4k.routing.routes

object AuthApiRoutes {
    fun routes(
        authService: AuthService,
        usersService: UsersService,
    ): RoutingHttpHandler =
        "/auth" bind
            routes(
                AuthApiV1Routes.routes(
                    usersService = usersService,
                    authService = authService,
                ),
            )
}
