package com.markkewley.auth

import com.markkewley.models.UserRequestContext

data class AuthenticationResult(val isAuthenticated: Boolean, val principal: UserRequestContext?, val error: String?)
