package com.markkewley.auth.models

import kotlinx.serialization.Serializable

@Serializable
data class VerifyIdTokenRequest(
    val idToken: String,
    val provider: SupportedProviders,
)
