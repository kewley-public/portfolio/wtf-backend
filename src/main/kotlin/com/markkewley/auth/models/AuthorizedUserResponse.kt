package com.markkewley.auth.models

import kotlinx.serialization.Serializable

@Serializable
data class AuthorizedUserResponse(
    val id: String,
    val name: String?,
    val accessToken: String,
    val expiresAt: Long,
)
