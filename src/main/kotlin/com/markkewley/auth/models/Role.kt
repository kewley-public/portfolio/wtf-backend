package com.markkewley.auth.models

enum class Role {
    VIEW,
    USER,
    ADMIN,
}
