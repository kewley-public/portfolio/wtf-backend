package com.markkewley.auth.models

import kotlinx.serialization.Serializable

@Serializable
data class RegisterAuthStrategyRequest(
    val email: String,
    val googleOAuthId: String? = null,
)
