package com.markkewley.auth.models

import kotlinx.serialization.Serializable

@Serializable
data class AuthorizeUserRequest(
    val email: String,
    val password: String,
)
