package com.markkewley.auth.models

import java.time.Instant

data class AuthUser(
    override val id: String,
    override val created: Instant,
    override val email: String,
    override val verified: Boolean,
    val verificationToken: String,
) : BaseUser
