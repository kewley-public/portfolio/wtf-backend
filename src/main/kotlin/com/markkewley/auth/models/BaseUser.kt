package com.markkewley.auth.models

import java.time.Instant

interface BaseUser {
    // Hash Key
    val id: String

    // Sort Key
    val created: Instant
    val email: String
    val verified: Boolean
}
