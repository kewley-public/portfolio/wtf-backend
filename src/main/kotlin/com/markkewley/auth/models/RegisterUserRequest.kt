package com.markkewley.auth.models

import kotlinx.serialization.Serializable

@Serializable
data class RegisterUserRequest(
    val email: String,
    val password: String? = null,
    val userName: String? = null,
    val googleOAuthId: String? = null,
)
