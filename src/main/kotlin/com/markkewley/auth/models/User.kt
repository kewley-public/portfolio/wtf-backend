package com.markkewley.auth.models

import java.time.Instant

data class User(
    override val id: String,
    override val created: Instant,
    override val email: String,
    override val verified: Boolean,
    val roles: List<Role> = listOf(),
    val userName: String? = null,
    val passwordHash: String? = null,
    val googleOAuthId: String? = null,
    val refreshToken: String? = null,
) : BaseUser
