package com.markkewley.auth.exception

/**
 * Exception that occurs while authorizing/authenticating a user.
 *
 * @param message - Some message that may be helpful to the user
 * (be thoughtful on not exposing too much here)
 */
data class AuthenticationException(override val message: String) : Exception(message)
