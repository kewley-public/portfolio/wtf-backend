package com.markkewley.auth.services

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.gson.GsonFactory
import com.markkewley.WheresTheFundsConfig
import com.markkewley.auth.models.RegisterUserRequest
import com.markkewley.auth.models.User
import com.markkewley.services.UsersService

class GoogleAuthService(
    private val usersService: UsersService,
) {
    private val verifier =
        GoogleIdTokenVerifier.Builder(NetHttpTransport(), GsonFactory())
            .setAudience(listOf(WheresTheFundsConfig.googleClientId))
            .build()

    fun verifyIdTokenAndRetrieveUser(idToken: String): User? {
        println("Verifying idToken $idToken")
        val token: GoogleIdToken?
        try {
            token = verifier.verify(idToken)
        } catch (exception: Exception) {
            println("Error occurred verifying idToken $idToken")
            throw exception
        }
        if (token != null) {
            println("Token retrieved for $idToken")
            val payload = token.payload
            if (payload.emailVerified) {
                return usersService.registerUser(
                    RegisterUserRequest(
                        email = payload.email,
                        googleOAuthId = payload.subject,
                    ),
                )
            }
            println("Email was not verified from google, avoiding registration of user")
        }
        println("Token response was null... unable to verify $idToken")
        return null
    }
}
