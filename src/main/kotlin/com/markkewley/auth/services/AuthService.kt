package com.markkewley.auth.services

import com.markkewley.WheresTheFundsConfig
import com.markkewley.auth.AuthenticationResult
import com.markkewley.auth.exception.AuthenticationException
import com.markkewley.auth.models.AuthorizeUserRequest
import com.markkewley.auth.models.AuthorizedUserResponse
import com.markkewley.auth.models.SupportedProviders
import com.markkewley.auth.models.User
import com.markkewley.auth.models.VerifyIdTokenRequest
import com.markkewley.models.UserRequestContext
import com.markkewley.services.UsersService
import org.http4k.core.Filter
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.core.with
import org.http4k.lens.RequestContextLens
import org.mindrot.jbcrypt.BCrypt
import java.time.Instant
import java.time.temporal.ChronoUnit

/**
 * Service providing authentication functionalities, integrating both Google OAuth and custom strategies.
 * It also handles the creation of JWT tokens for authenticated users.
 */
class AuthService(private val usersService: UsersService) {
    private val googleAuthService = GoogleAuthService(usersService)

    /**
     * Creates an authentication filter that can be applied to routes to enforce authentication.
     * It checks the authentication strategy (Google OAuth or custom) that the request belongs to and authenticates accordingly.
     *
     * @param key A RequestContextLens used to attach the authenticated user to the request context.
     * @return A Filter that can be applied to routes.
     */
    fun requireAuthFilter(key: RequestContextLens<UserRequestContext>) =
        Filter { next ->
            { request ->
                val authenticationResult = authenticate(request)
                if (authenticationResult.isAuthenticated && authenticationResult.principal != null) {
                    next(request.with(key of authenticationResult.principal))
                } else {
                    Response(Status.UNAUTHORIZED)
                }
            }
        }

    private fun authenticate(request: Request): AuthenticationResult {
        val token = request.header("Authorization")?.removePrefix("Bearer ")
        if (token != null) {
            val decodedToken = TokenService.validateToken(token)
            if (decodedToken != null) {
                val userId = decodedToken.subject
                return AuthenticationResult(
                    isAuthenticated = true,
                    principal = UserRequestContext(userId = userId, roles = listOf()),
                    error = null,
                )
            }
        }
        val errorMessage = if (token == null) "Missing Authorization Token" else "Invalid Authorization Token"
        return AuthenticationResult(isAuthenticated = false, principal = null, error = errorMessage)
    }

    fun verifyIdToken(verifyIdTokenRequest: VerifyIdTokenRequest): Pair<AuthorizedUserResponse, String> {
        val user =
            when (verifyIdTokenRequest.provider) {
                SupportedProviders.GOOGLE ->
                    googleAuthService.verifyIdTokenAndRetrieveUser(
                        idToken = verifyIdTokenRequest.idToken,
                    )
            } ?: throw AuthenticationException("User not found or token invalid")

        val refreshToken = refreshRefreshTokenIfNeeded(user = user)

        val expiresAt = determineExpiresAt()
        val accessToken = TokenService.generateAccessToken(user = user, expiresAt = expiresAt)

        return Pair(
            AuthorizedUserResponse(
                id = user.id,
                name = user.userName,
                accessToken = accessToken,
                expiresAt = expiresAt.toEpochMilli(),
            ),
            refreshToken,
        )
    }

    fun verifyEmailAndPassword(authorizeUserRequest: AuthorizeUserRequest): Pair<AuthorizedUserResponse, String> {
        val user =
            usersService.getUserByEmail(email = authorizeUserRequest.email)
                ?: throw AuthenticationException("Invalid email or password")

        if (!BCrypt.checkpw(authorizeUserRequest.password, user.passwordHash)) {
            throw AuthenticationException("Invalid email or password")
        }

        val refreshToken = refreshRefreshTokenIfNeeded(user = user)

        val expiresAt = determineExpiresAt()
        val accessToken = TokenService.generateAccessToken(user = user, expiresAt = expiresAt)

        return Pair(
            AuthorizedUserResponse(
                id = user.id,
                name = user.userName,
                accessToken = accessToken,
                expiresAt = expiresAt.toEpochMilli(),
            ),
            refreshToken,
        )
    }

    fun updateSession(refreshToken: String): AuthorizedUserResponse {
        TokenService.validateToken(refreshToken)
            ?: throw AuthenticationException("Refresh token invalid or has expired")

        val user =
            usersService.getUserByRefreshToken(refreshToken = refreshToken)
                ?: throw AuthenticationException("Refresh token invalid or has expired")

        val expiresAt = determineExpiresAt()
        val accessToken = TokenService.generateAccessToken(user = user, expiresAt = expiresAt)

        return AuthorizedUserResponse(
            id = user.id,
            name = user.userName,
            accessToken = accessToken,
            expiresAt = expiresAt.toEpochMilli(),
        )
    }

    private fun refreshRefreshTokenIfNeeded(user: User): String {
        val validToken = TokenService.validateToken(token = user.refreshToken)
        if (validToken == null) {
            val newToken = TokenService.generateRefreshToken(userId = user.id)
            usersService.saveRefreshToken(user = user, refreshToken = newToken)
            return newToken
        }
        return validToken.token
    }

    private fun determineExpiresAt(): Instant = Instant.now().plus(WheresTheFundsConfig.jwtExpirationInMinutes, ChronoUnit.MINUTES)
}
