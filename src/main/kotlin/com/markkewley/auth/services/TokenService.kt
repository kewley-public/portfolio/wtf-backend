package com.markkewley.auth.services

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.JWTVerificationException
import com.auth0.jwt.interfaces.DecodedJWT
import com.markkewley.WheresTheFundsConfig
import com.markkewley.auth.models.BaseUser
import org.http4k.core.Request
import org.http4k.core.cookie.Cookie
import org.http4k.core.cookie.cookie
import java.time.Instant
import java.time.temporal.ChronoUnit

object TokenService {
    /**
     * Generates a JWT access token for a given user. The token includes claims such as issuer, subject (user ID),
     * user roles, and an expiration time.
     *
     * @param user The User object for whom the token is to be generated.
     * @param expiresAt when the access token expires
     * @return The generated JWT token as a String.
     */
    fun generateAccessToken(
        user: BaseUser,
        expiresAt: Instant,
    ): String {
        val algorithm = Algorithm.HMAC256(WheresTheFundsConfig.jwtSecret) // Use the same secret key

        val token =
            JWT.create()
                .withIssuer(WheresTheFundsConfig.appName)
                .withSubject(user.id)
                .withExpiresAt(expiresAt) // Set expiration time, e.g., 1 hour from now
                .sign(algorithm)

        return token
    }

    /**
     * Creates a refresh token for a provided user. This is utilized when regenerating access tokens
     * so the user doesn't have to re-authenticate after an AccessToken has expired.
     *
     * Once this token has expired it's advised the user re-authenticates.
     *
     * @param userId The user identifier for whom this refresh token will belong too
     * @return The generated JWT Refresh Token as a String
     */
    fun generateRefreshToken(userId: String): String {
        val algorithm = Algorithm.HMAC256(WheresTheFundsConfig.jwtSecret) // Use the same secret key

        val token =
            JWT.create()
                .withIssuer(WheresTheFundsConfig.appName)
                .withSubject(userId)
                .withExpiresAt(Instant.now().plus(7, ChronoUnit.DAYS))
                .sign(algorithm)

        return token
    }

    /**
     * Verifies if the token being provided is valid with our system and has
     * not expired.
     *
     * @param token The token in question to decode
     */
    fun validateToken(token: String?): DecodedJWT? {
        try {
            val algorithm =
                Algorithm.HMAC256(WheresTheFundsConfig.jwtSecret)
            val verifier =
                JWT.require(algorithm)
                    .withIssuer(WheresTheFundsConfig.appName)
                    .build()
            return verifier.verify(token)
        } catch (exception: JWTVerificationException) {
            // Invalid signature/claims
            println(exception.localizedMessage)
            return null
        }
    }

    fun getRefreshTokenCookieFromRequest(request: Request) = request.cookie("refreshToken")?.value

    fun generateRefreshTokenCookie(refreshToken: String): Cookie =
        Cookie(
            name = "refreshToken",
            value = refreshToken,
            httpOnly = true,
            // Set to 'true' to send the cookie over HTTPS only
            secure = !WheresTheFundsConfig.runLocalDynamoDb,
            // 1 Week
            maxAge = 3600 * 24 * 7,
        )
}
