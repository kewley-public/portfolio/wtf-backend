package com.markkewley

import com.markkewley.serializers.CORE_JSON
import io.github.cdimascio.dotenv.dotenv
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClient
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueRequest
import java.util.WeakHashMap

object WheresTheFundsConfig {
    private val dotenv =
        dotenv {
            ignoreIfMalformed = true
            ignoreIfMissing = true
            filename =
                when {
                    System.getProperty("RUNNING_TESTS") == "true" -> ".env.test"
                    System.getenv("STAGE") != null -> ".env.${System.getenv("STAGE")}"
                    else -> ".env"
                }
        }

    // Cache for the secret values
    private var secretValues: WeakHashMap<String, String>? = null

    // AWS Secrets Manager client
    private val secretsClient by lazy { SecretsManagerClient.create() }

    // Function to fetch secret value from AWS Secrets Manager
    private fun fetchSecret(secretArn: String): WeakHashMap<String, String> {
        val request = GetSecretValueRequest.builder().secretId(secretArn).build()
        val response = secretsClient.getSecretValue(request)
        val secretString = response.secretString()
        val json = CORE_JSON.parseToJsonElement(secretString).jsonObject
        return WeakHashMap(json.mapValues { it.value.jsonPrimitive.content })
    }

    // Get secret value or fallback to dotenv and environment variables
    private fun getSecretValue(key: String): String? {
        val secretArn = dotenv.get("SECRET_ARN") ?: System.getenv("SECRET_ARN")
        if (secretArn != null) {
            if (secretValues == null) {
                secretValues = fetchSecret(secretArn)
            }
            return secretValues?.get(key)
        }
        return dotenv.get(key) ?: System.getenv(key)
    }

    // Rate Limit Setup
    // How many requests we allow to the application
    val rateLimitRequestLimit =
        dotenv.get("RATE_LIMIT_REQUEST_LIMIT") ?: System.getenv("RATE_LIMIT_REQUEST_LIMIT") ?: "1000"

    // Allowed period for requests
    val rateLimitRefreshPeriodInSeconds =
        dotenv.get("RATE_LIMIT_REFRESH_PERIOD_IN_SECONDS") ?: System.getenv("RATE_LIMIT_REFRESH_PERIOD_IN_SECONDS")
            ?: "5"

    // When a user hits the rate limit how long we hold the request to prevent them from spamming more
    val rateLimitTimeoutInMillis =
        dotenv.get("RATE_LIMIT_TIMEOUT_MILLIS") ?: System.getenv("RATE_LIMIT_TIMEOUT_MILLIS") ?: "100"

    val enableCors = (dotenv.get("ENABLE_CORS") ?: System.getenv("ENABLE_CORS") ?: "false").toBoolean()

    // AWS Setup
    val awsRegion =
        dotenv.get("AWS_REGION") ?: System.getenv("AWS_REGION") ?: "us-east-1" // Default to 'us-east-1' if not set

    // Dynamo
    const val PRIMARY_KEY = "PK"
    const val SORT_KEY = "SK"
    const val GLOBAL_SECONDARY_INDEX_ONE_NAME = "GSI1"
    const val GLOBAL_SECONDARY_INDEX_TWO_NAME = "GSI2"
    const val GLOBAL_SECONDARY_INDEX_THREE_NAME = "GSI3"
    const val ENTITY_TYPE_INDEX_NAME = "EntityType"
    const val GLOBAL_SECONDARY_INDEX_ONE_PRIMARY_KEY = "GSI1-PK"
    const val GLOBAL_SECONDARY_INDEX_ONE_SORT_KEY = "GSI1-SK"
    const val GLOBAL_SECONDARY_INDEX_TWO_PRIMARY_KEY = "GSI2-PK"
    const val GLOBAL_SECONDARY_INDEX_TWO_SORT_KEY = "GSI2-SK"
    const val GLOBAL_SECONDARY_INDEX_THREE_PRIMARY_KEY = "GSI3-PK"
    const val GLOBAL_SECONDARY_INDEX_THREE_SORT_KEY = "GSI3-SK"
    const val ENTITY_TYPE_PRIMARY_KEY = "EntityType-PK"
    const val ENTITY_TYPE_SORT_KEY = "EntityType-SK"

    val primaryTableName =
        dotenv.get("PRIMARY_TABLE_NAME") ?: System.getenv("PRIMARY_TABLE_NAME")
            ?: throw java.lang.RuntimeException("PRIMARY_TABLE_NAME not set in environment")
    val categoryEntityName =
        dotenv.get("ENTITY_NAME_CATEGORY") ?: System.getenv("ENTITY_NAME_CATEGORY")
            ?: throw java.lang.RuntimeException("ENTITY_NAME_CATEGORY not set in environment")
    val expenseEntityName =
        dotenv.get("ENTITY_NAME_EXPENSE") ?: System.getenv("ENTITY_NAME_EXPENSE")
            ?: throw java.lang.RuntimeException("ENTITY_NAME_EXPENSE not set in environment")
    val expenseEventEntityName =
        dotenv.get("ENTITY_NAME_EVENT") ?: System.getenv("ENTITY_NAME_EVENT")
            ?: throw java.lang.RuntimeException("ENTITY_NAME_EVENT not set in environment")
    val fundEntityName =
        dotenv.get("ENTITY_NAME_FUND") ?: System.getenv("ENTITY_NAME_FUND")
            ?: throw java.lang.RuntimeException("ENTITY_NAME_FUND not set in environment")
    val userEntityName =
        dotenv.get("ENTITY_NAME_USER") ?: System.getenv("ENTITY_NAME_USER")
            ?: throw java.lang.RuntimeException("ENTITY_NAME_USER not set in environment")

    // For local dynamo (see docker-compose.yml for setup)
    val runLocalDynamoDb = (dotenv.get("RUN_LOCAL_DYNAMO_DB") ?: "false").toBoolean()

    // AUTH
    val appName = dotenv.get("APP_NAME") ?: System.getenv("APP_NAME") ?: "WheresTheFunds"

    // Used for jbcrypt for pass encryption
    val saltRounds: Int = (dotenv.get("SALT_ROUNDS") ?: System.getenv("SALT_ROUNDS") ?: "12").toInt()

    // Custom
    val jwtSecret: String =
        getSecretValue("JWT_SECRET")
            ?: throw RuntimeException("JWT_SECRET not set in environment")
    val jwtExpirationInMinutes: Long =
        (dotenv.get("EXPIRATION_IN_MINUTES") ?: System.getenv("EXPIRATION_IN_MINUTES") ?: "120").toLong()

    // OAuth
    val googleClientId: String =
        getSecretValue("GOOGLE_CLIENT_ID")
            ?: throw RuntimeException("GOOGLE_CLIENT_ID not set in environment")

    // SQS
    val fundSqsQueueUrl =
        dotenv.get("FUND_SQS_URL") ?: System.getenv("FUND_SQS_URL")
            ?: throw RuntimeException("FUND_SQS_URL not set in environment")
    val fundSqsQueueArn =
        dotenv.get("FUND_SQS_ARN") ?: System.getenv("FUND_SQS_ARN")
            ?: throw RuntimeException("FUND_SQS_ARN not set in environment")
}
