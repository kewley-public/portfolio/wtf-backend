package com.markkewley

import org.http4k.core.Filter
import org.http4k.core.Method
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import org.http4k.core.with
import org.http4k.lens.Header

object CorsFilter {
    private const val ORIGIN = "Origin"

    fun create(): Filter =
        Filter { nextHandler ->
            { request ->
                val response =
                    if (request.method == Method.OPTIONS) {
                        Response(OK)
                    } else {
                        nextHandler(request)
                    }

                response.with(
                    Header.required("access-control-allow-origin") of (request.header(ORIGIN) ?: "*"),
                    Header.required("access-control-allow-methods") of "GET, POST, PUT, DELETE, OPTIONS",
                    Header.required("access-control-allow-headers") of "Content-Type, Authorization",
                    Header.required("access-control-allow-credentials") of "true",
                )
            }
        }
}
