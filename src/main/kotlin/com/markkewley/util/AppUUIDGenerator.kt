package com.markkewley.util

import com.github.f4b6a3.uuid.UuidCreator
import java.time.Instant

object AppUUIDGenerator {
    /**
     * UUIDv7: https://github.com/f4b6a3/uuid-creator/wiki/1.7.-UUIDv7
     *
     * Allows for time based sorting which is useful for Dynamo sort keus.
     */
    fun generateUUID(instant: Instant? = null) =
        if (instant != null) UuidCreator.getTimeOrderedEpoch(instant) else UuidCreator.getTimeOrderedEpoch()
}
