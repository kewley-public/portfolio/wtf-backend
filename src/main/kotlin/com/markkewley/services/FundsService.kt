package com.markkewley.services

import com.markkewley.WheresTheFundsConfig
import com.markkewley.exception.InvalidRequestException
import com.markkewley.exception.WheresTheFundsException
import com.markkewley.models.Category
import com.markkewley.models.CreateFundRequest
import com.markkewley.models.Fund
import com.markkewley.models.InitialCreateCategory
import com.markkewley.util.AppUUIDGenerator
import software.amazon.awssdk.services.dynamodb.DynamoDbClient
import software.amazon.awssdk.services.dynamodb.model.AttributeValue
import software.amazon.awssdk.services.dynamodb.model.BatchWriteItemRequest
import software.amazon.awssdk.services.dynamodb.model.PutRequest
import software.amazon.awssdk.services.dynamodb.model.WriteRequest

class FundsService(dynamoDbClient: DynamoDbClient, private val categoriesService: CategoriesService) :
    DynamoService<Fund>(dynamoDbClient) {
    private val categoryIdIndexName = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_ONE_NAME
    private val categoryIdPrimaryKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_ONE_PRIMARY_KEY
    private val categoryIdSortKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_ONE_SORT_KEY

    /**
     * Will retrieve all [Fund]s for a given user.
     *
     * I'm assuming we will never exceed the 10MB limit since there won't be that many categories
     * and the category object itself is quite small.
     *
     * @param userId - UUID representing a User
     * @return [List<[Category]>] - All [Category]'s belonging to the user
     */
    fun getAllFundsForUser(userId: String) =
        queryItems<Fund>(
            keyConditionExpression = "#pk = :userId and begins_with(#sk, :sortKeyPrefix)",
            expressionAttributeNames =
                mapOf(
                    "#pk" to primaryKey,
                    "#sk" to sortKey,
                ),
            expressionAttributeValues =
                mapOf(
                    ":userId" to
                        AttributeValue.builder().s(buildItemId(entityName = userEntityName, itemId = userId))
                            .build(),
                    ":sortKeyPrefix" to AttributeValue.builder().s("$fundEntityName#").build(),
                ),
        ).items

    /**
     * Will retrieve a single [Fund] by a [userId] and [fundId].
     *
     * @param userId - UUID representing a User
     * @param fundId - UUID representing a [Fund]
     * @return [Fund] if it exists
     */
    private fun getFundById(
        userId: String,
        fundId: String,
    ) = getItemById<Fund>(
        userId = userId,
        itemId = fundId,
        entityName = fundEntityName,
    )

    /**
     * Creates default [Fund]s for a new user.
     *
     * @param userId The identifier of the user for whom the default categories are being created.
     * @param initialCategories collection of initial [Category] objects created for a user
     */
    fun createInitialFunds(
        userId: String,
        initialCategories: List<InitialCreateCategory>,
    ) {
        val writeRequest =
            initialCategories.map { category ->
                WriteRequest.builder()
                    .putRequest(
                        PutRequest.builder()
                            .item(
                                buildCreationMap(
                                    itemId = category.fundId,
                                    userId = userId,
                                    categoryId = category.itemId,
                                    description = category.description,
                                    amount = 100.0,
                                ),
                            )
                            .build(),
                    )
                    .build()
            }

        val batchWriteItemRequest =
            BatchWriteItemRequest.builder()
                .requestItems(mapOf(tableName to writeRequest))
                .build()

        try {
            dynamoDbClient.batchWriteItem(batchWriteItemRequest)
        } catch (e: Exception) {
            val message = "Failed to create initial Funds for user[$userId] ${e.localizedMessage}"
            println(message)
            throw WheresTheFundsException(message)
        }
    }

    /**
     * Creates a [Fund] record in our database.
     *
     * @param createFundRequest [CreateFundRequest] object which holds onto necessary data for a [Fund] creation
     * @param userId The identifier of the user which all Funds are keyed by
     * @throws [InvalidRequestException] when the [CreateFundRequest.categoryId] doesn't exist OR when the specific
     * category already belongs to a [Fund]
     */
    fun createFund(
        createFundRequest: CreateFundRequest,
        userId: String,
    ): Fund {
        val category =
            categoriesService.getCategoryById(userId = userId, categoryId = createFundRequest.categoryId)
                ?: throw InvalidRequestException("Category does not exist")

        if (category.fundId != null) {
            throw InvalidRequestException("Fund already exists for category")
        }

        val itemValues =
            buildCreationMap(
                userId = userId,
                categoryId = createFundRequest.categoryId,
                description = createFundRequest.description,
                amount = createFundRequest.amount,
            )

        val newFund =
            createItem<Fund>(
                userId = userId,
                itemId = extractItemId(itemValues[sortKey]?.s()),
                itemValues = itemValues,
                entityName = fundEntityName,
            )

        val fundId =
            extractItemId(itemId = itemValues[sortKey]?.s()).ifEmpty {
                deleteItem(userId = userId, itemId = "", entityName = fundEntityName)
                throw WheresTheFundsException("Error creating fund")
            }

        val categoryUpdated =
            categoriesService.assignCategoryToFund(userId = userId, categoryId = category.id, fundId = fundId)
        if (categoryUpdated) {
            return newFund
        }
        println("Unable to update category [${category.id}]")
        deleteItem(userId = userId, itemId = fundId, entityName = fundEntityName)
        throw WheresTheFundsException("Unable to create fund")
    }

    /**
     * Permanently removes a [Fund] from the database.
     *
     * @param userId The identifier of the user which all Funds are keyed by
     * @param userId The identifier of the [Fund] for which we wish to delete
     * @return T if success, F otherwise
     */
    fun deleteFund(
        userId: String,
        fundId: String,
    ) = deleteItem(userId = userId, itemId = fundId, entityName = fundEntityName)

    /**
     * Retrieves a Fund if exists for a given category. There can be only one Fund per
     * category.
     *
     * @param categoryId - UUID for the category
     * @return a Fund if it exists for the given category
     */
    fun getFundByCategory(categoryId: String): Fund? {
        val categoryIdForDynamo =
            buildIndexItemId(
                entityName = fundEntityName,
                indexEntityName = categoryEntityName,
                indexItemId = categoryId,
            )
        return querySingleItem<Fund>(
            keyConditionExpression = "#pk = :pk and #sk = :sk",
            expressionAttributeNames =
                mapOf(
                    "#pk" to categoryIdPrimaryKey,
                    "#sk" to categoryIdSortKey,
                ),
            expressionAttributeValues =
                mapOf(
                    ":pk" to AttributeValue.builder().s(categoryIdForDynamo).build(),
                    ":sk" to AttributeValue.builder().s(categoryIdForDynamo).build(),
                ),
            indexName = categoryIdIndexName,
        ).first
    }

    /**
     * Converts a DynamoDB item map to a [Fund] object.
     *
     * This method is an implementation of the abstract method defined in DynamoService. It extracts attribute values
     * from a DynamoDB item map and constructs a [Fund] object.
     *
     * @param item The map of attribute names to [AttributeValue] objects representing a fund item in DynamoDB.
     * @return A [Fund] object constructed from the item map, or null if required attributes are missing.
     */
    override fun toItem(item: Map<String, AttributeValue>): Fund? {
        val fundItemId = extractItemId(itemId = item[sortKey]?.s())
        val categoryId = extractItemId(itemId = item[categoryIdPrimaryKey]?.s())

        val description = item["description"]?.s() ?: return null
        val amount = item["amount"]?.n()?.toDoubleOrNull() ?: return null

        return Fund(
            id = fundItemId.ifEmpty { return null },
            description = description,
            amount = amount,
            categoryId = categoryId.ifEmpty { return null },
        )
    }

    private fun buildCreationMap(
        itemId: String? = null,
        userId: String,
        categoryId: String,
        description: String,
        amount: Double,
    ): Map<String, AttributeValue> {
        val userIdForDynamo = buildItemId(entityName = userEntityName, itemId = userId)
        val fundIdForDynamo =
            buildItemId(entityName = fundEntityName, itemId = itemId ?: AppUUIDGenerator.generateUUID().toString())
        val categoryIdForDynamo =
            buildIndexItemId(
                entityName = fundEntityName,
                indexEntityName = categoryEntityName,
                indexItemId = categoryId,
            )
        return mapOf(
            primaryKey to AttributeValue.builder().s(userIdForDynamo).build(),
            sortKey to AttributeValue.builder().s(fundIdForDynamo).build(),
            "description" to AttributeValue.builder().s(description).build(),
            "amount" to AttributeValue.builder().n(amount.toString()).build(),
            categoryIdPrimaryKey to AttributeValue.builder().s(categoryIdForDynamo).build(),
            categoryIdSortKey to AttributeValue.builder().s(categoryIdForDynamo).build(),
            entityTypePrimaryKey to AttributeValue.builder().s(fundEntityName).build(),
            entityTypeSortKey to AttributeValue.builder().s(userIdForDynamo).build(),
        )
    }
}
