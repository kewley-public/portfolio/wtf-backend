package com.markkewley.services

import com.markkewley.WheresTheFundsConfig
import com.markkewley.auth.models.AuthUser
import com.markkewley.auth.models.BaseUser
import com.markkewley.auth.models.RegisterAuthStrategyRequest
import com.markkewley.auth.models.RegisterUserRequest
import com.markkewley.auth.models.Role
import com.markkewley.auth.models.User
import com.markkewley.exception.InvalidRequestException
import com.markkewley.exception.NotFoundException
import com.markkewley.exception.WheresTheFundsException
import com.markkewley.models.InitialCreateCategory
import com.markkewley.models.UserResponse
import com.markkewley.util.AppUUIDGenerator
import org.mindrot.jbcrypt.BCrypt
import software.amazon.awssdk.services.dynamodb.DynamoDbClient
import software.amazon.awssdk.services.dynamodb.model.AttributeValue
import software.amazon.awssdk.services.dynamodb.model.UpdateItemRequest
import java.time.Instant

/**
 * Provides functionalities to interact with user data stored in a DynamoDB table.
 * Allows operations such as retrieving, registering, updating, and verifying users.
 *
 * @param dynamoDbClient An instance of DynamoDbClient used to perform operations on the DynamoDB table.
 */
class UsersService(
    dynamoDbClient: DynamoDbClient,
    private val categoriesService: CategoriesService,
    private val fundsService: FundsService,
) : DynamoService<User>(dynamoDbClient) {
    private val emailIndexName = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_ONE_NAME
    private val emailPrimaryKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_ONE_PRIMARY_KEY
    private val emailSortKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_ONE_SORT_KEY

    private val refreshTokenIndexName = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_TWO_NAME
    private val refreshTokenPrimaryKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_TWO_PRIMARY_KEY
    private val refreshTokenSortKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_TWO_SORT_KEY

    private val verificationTokenIndexName = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_THREE_NAME
    private val verificationTokenPrimaryKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_THREE_PRIMARY_KEY
    private val verificationTokenSortKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_THREE_SORT_KEY

    /**
     * Retrieves a user by their refreshToken
     *
     * @param refreshToken The refresh token passed in from a cookie
     * @return A User object if found, otherwise null.
     */
    fun getUserByRefreshToken(refreshToken: String) =
        querySingleItem(
            keyConditionExpression = buildIndexKey(primaryKeyAttribute = ":refreshToken"),
            expressionAttributeNames =
                buildIndexAttributeNamesForPrimaryAndSortKey(
                    refreshTokenPrimaryKey,
                    refreshTokenSortKey,
                ),
            expressionAttributeValues =
                mapOf(
                    ":refreshToken" to AttributeValue.builder().s(refreshToken).build(),
                ),
            indexName = refreshTokenIndexName,
            toItemFn = { toUserResponseItem(item = it) },
        ).first

    /**
     * Retrieves the full user data by email.
     *
     * DO NOT return this to the Client
     *
     * @param email The email address of the user.
     * @return A AuthenticateUser object if found, otherwise null.
     */
    fun getUserByEmail(email: String) =
        querySingleItem<User>(
            keyConditionExpression = buildIndexKey(":email"),
            expressionAttributeNames = buildIndexAttributeNamesForPrimaryAndSortKey(emailPrimaryKey, emailSortKey),
            expressionAttributeValues =
                mapOf(
                    ":email" to
                        AttributeValue.builder().s(buildItemId(entityName = userEntityName, itemId = email))
                            .build(),
                ),
            indexName = emailIndexName,
        ).first

    /**
     * Retrieves user response by id for the client to view
     * profile details.
     *
     * @param id The user identifier
     * @return A UserResponse object if found, otherwise null
     */
    fun getUserById(id: String) =
        getItemById(
            userId = id,
            itemId = id,
            entityName = userEntityName,
            toItemFn = { toUserResponseItem(item = it) },
        )

    /**
     * Retrieves a user by their verification token.
     *
     * @param token The verification token associated with the user.
     * @return A User object if found, otherwise null.
     */
    fun getUserByVerificationToken(token: String) =
        querySingleItem<UserResponse>(
            keyConditionExpression = buildIndexKey(":verificationToken"),
            expressionAttributeNames =
                buildIndexAttributeNamesForPrimaryAndSortKey(
                    verificationTokenPrimaryKey,
                    verificationTokenSortKey,
                ),
            expressionAttributeValues =
                mapOf(
                    ":verificationToken" to AttributeValue.builder().s(token).build(),
                ),
            indexName = verificationTokenIndexName,
        ).first

    /**
     * Registers a new user in the DynamoDB table.
     *
     * @param registerUserRequest An object containing user registration details.
     * @return A User object representing the newly created user, or null if registration fails.
     */
    fun registerUser(registerUserRequest: RegisterUserRequest): User {
        println("Registering user ${registerUserRequest.email}...")
        val user = getUserByEmail(email = registerUserRequest.email)
        if (user != null) {
            println("User exists")

            if (user.googleOAuthId == null && registerUserRequest.googleOAuthId != null) {
                return registerAuthStrategy(
                    registerAuthStrategyRequest =
                        RegisterAuthStrategyRequest(
                            email = user.email,
                            googleOAuthId = registerUserRequest.googleOAuthId,
                        ),
                )
            }
            return user
        }

        if (registerUserRequest.password == null && registerUserRequest.googleOAuthId == null) {
            println("Invalid Registration details")
            throw InvalidRequestException("Invalid Registration details")
        }

        println("User [${registerUserRequest.email}] did not exists, creating...")

        val itemId = AppUUIDGenerator.generateUUID().toString()
        val verificationToken = AppUUIDGenerator.generateUUID().toString()

        val emailId = buildItemId(entityName = userEntityName, itemId = registerUserRequest.email)
        val userId = buildItemId(entityName = userEntityName, itemId = itemId)
        val created = Instant.now()
        val itemValues =
            mutableMapOf<String, AttributeValue>(
                primaryKey to AttributeValue.builder().s(userId).build(),
                sortKey to AttributeValue.builder().s(userId).build(),
                "created" to AttributeValue.builder().s(created.toString()).build(),
                "verified" to AttributeValue.builder().bool(false).build(),
                "roles" to
                    AttributeValue.builder()
                        .l(
                            AttributeValue.builder().s(Role.VIEW.name).build(),
                            AttributeValue.builder().s(Role.USER.name).build(),
                        ).build(),
                emailPrimaryKey to AttributeValue.builder().s(emailId).build(),
                emailSortKey to AttributeValue.builder().s(emailId).build(),
                verificationTokenPrimaryKey to AttributeValue.builder().s(verificationToken).build(),
                verificationTokenSortKey to AttributeValue.builder().s(verificationToken).build(),
                entityTypePrimaryKey to AttributeValue.builder().s(userEntityName).build(),
                entityTypeSortKey to AttributeValue.builder().s(created.toString()).build(),
            ).apply {
                registerUserRequest.password?.let {
                    this["passwordHash"] =
                        AttributeValue.builder().s(BCrypt.hashpw(it, BCrypt.gensalt(WheresTheFundsConfig.saltRounds)))
                            .build()
                }
                registerUserRequest.googleOAuthId?.let {
                    this["googleOAuthId"] = AttributeValue.builder().s(it).build()
                }
                registerUserRequest.userName?.let {
                    this["userName"] = AttributeValue.builder().s(it).build()
                }
            }

        val createdUser = createItem<User>(userId = itemId, itemId = itemId, itemValues = itemValues, entityName = userEntityName)
        val categories: List<InitialCreateCategory>
        try {
            categories = categoriesService.createInitialCategoriesForUser(userId = createdUser.id)
        } catch (e: Exception) {
            println("Error creating initial categories ${e.localizedMessage}")
            deleteItem(itemId = createdUser.id, userId = createdUser.id, entityName = userEntityName)
            throw WheresTheFundsException("Unable to create initial categories")
        }

        try {
            fundsService.createInitialFunds(userId = createdUser.id, initialCategories = categories)
        } catch (e: Exception) {
            // Not worried as much about initial funds, user can easily create these
            println("Error creating initial funds ${e.localizedMessage}")
        }
        return createdUser
    }

    /**
     * Updates the authentication strategy for an existing user.
     *
     * @param registerAuthStrategyRequest An object containing the new authentication details for the user.
     * @return true if the operation was successful, false otherwise.
     */
    fun registerAuthStrategy(registerAuthStrategyRequest: RegisterAuthStrategyRequest): User {
        val user = getUserByEmail(email = registerAuthStrategyRequest.email) ?: throw NotFoundException(message = "User not found")
        val updateItemRequestBuilder = UpdateItemRequest.builder().tableName(tableName)
        val updateItemRequest =
            when {
                registerAuthStrategyRequest.googleOAuthId != null -> {
                    updateItemRequestBuilder
                        .key(buildUserIdKey(user.id))
                        .updateExpression("SET #oAuthId = :oAuthId")
                        .expressionAttributeNames(mapOf("#oAuthId" to "googleOAuthId"))
                        .expressionAttributeValues(
                            mapOf(
                                ":oAuthId" to
                                    AttributeValue.builder().s(registerAuthStrategyRequest.googleOAuthId)
                                        .build(),
                            ),
                        )
                        .build()
                }

                else -> throw InvalidRequestException("Auth strategy not supported")
            }

        try {
            dynamoDbClient.updateItem(updateItemRequest)
            println("Successfully registered auth strategy for user ${user.id} - ${user.email}")
            return getUserByEmail(email = registerAuthStrategyRequest.email)
                ?: throw WheresTheFundsException(message = "Unable to retrieve user after registration")
        } catch (e: Exception) {
            val message = "Unable to register auth strategy for user ${user.id} - ${user.email}"
            println("$message - ${e.localizedMessage}")
            throw WheresTheFundsException(message = message)
        }
    }

    /**
     * Verifies a user based on the provided verification token.
     *
     * @param verificationToken The token used to verify the user.
     * @return true if the user is successfully verified, false otherwise.
     */
    fun verifyUser(verificationToken: String): Boolean {
        val authUser =
            querySingleItem(
                keyConditionExpression =
                    buildIndexKey(
                        ":verificationToken",
                    ),
                expressionAttributeNames =
                    buildIndexAttributeNamesForPrimaryAndSortKey(
                        verificationTokenPrimaryKey,
                        verificationTokenSortKey,
                    ),
                expressionAttributeValues =
                    mapOf(
                        ":verificationToken" to AttributeValue.builder().s(verificationToken).build(),
                    ),
                indexName = verificationTokenIndexName,
                toItemFn = { toAuthUser(it) },
            ).first

        if (authUser?.verificationToken == verificationToken) {
            val updateItemRequest =
                UpdateItemRequest.builder()
                    .tableName(tableName)
                    .key(buildUserIdKey(authUser.id))
                    .updateExpression("SET verified = :verified")
                    .expressionAttributeValues(mapOf(":verified" to AttributeValue.builder().bool(true).build()))
                    .build()

            try {
                dynamoDbClient.updateItem(updateItemRequest)
                println("Successfully verified user ${authUser.id} - ${authUser.email}")
                return true
            } catch (e: Exception) {
                println("Error setting verification status on user ${authUser.id} - ${authUser.email}")
                e.printStackTrace()
            }
        }
        println("No user found with token $verificationToken")
        return false
    }

    fun saveRefreshToken(
        user: BaseUser,
        refreshToken: String,
    ): Boolean {
        val updateItemRequest =
            UpdateItemRequest.builder()
                .tableName(tableName)
                .key(buildUserIdKey(user.id))
                .updateExpression("SET #pk = :rt, #sk = :rt")
                .expressionAttributeNames(
                    buildIndexAttributeNamesForPrimaryAndSortKey(
                        refreshTokenPrimaryKey,
                        refreshTokenSortKey,
                    ),
                )
                .expressionAttributeValues(mapOf(":rt" to AttributeValue.builder().s(refreshToken).build()))
                .build()

        try {
            dynamoDbClient.updateItem(updateItemRequest)
            println("Successfully updated refresh token for user ${user.id}")
            return true
        } catch (e: Exception) {
            println("Error updating refresh token for user ${user.id} - ${e.localizedMessage}")
        }
        return false
    }

    private fun buildUserIdKey(userId: String): Map<String, AttributeValue> {
        val userIdMapped = buildItemId(entityName = userEntityName, itemId = userId)
        return buildKey(
            primaryKeyName = primaryKey,
            sortKeyName = sortKey,
            primaryKeyValue = userIdMapped,
            sortKeyValue = userIdMapped,
        )
    }

    private fun buildKey(
        primaryKeyName: String,
        sortKeyName: String,
        primaryKeyValue: String,
        sortKeyValue: String,
    ) = mapOf(
        primaryKeyName to AttributeValue.builder().s(primaryKeyValue).build(),
        sortKeyName to AttributeValue.builder().s(sortKeyValue).build(),
    )

    private fun buildIndexKey(
        primaryKeyAttribute: String,
        sortKeyAttribute: String? = null,
    ): String {
        val sortKeyValueParsed = sortKeyAttribute ?: primaryKeyAttribute
        return "#pk = $primaryKeyAttribute and #sk = $sortKeyValueParsed"
    }

    private fun buildIndexAttributeNamesForPrimaryAndSortKey(
        primaryKeyName: String,
        secondaryKeyName: String,
    ) = mapOf(
        "#pk" to primaryKeyName,
        "#sk" to secondaryKeyName,
    )

    /**
     * Converts a DynamoDB item map to a [User] object.
     *
     * This method is an implementation of the abstract method defined in DynamoService. It extracts attribute values
     * from a DynamoDB item map and constructs a [User] object.
     *
     * @param item The map of attribute names to [AttributeValue] objects representing a user item in DynamoDB.
     * @return A [User] object constructed from the item map, or null if required attributes are missing.
     */
    override fun toItem(item: Map<String, AttributeValue>): User? {
        val dateAsString = item["created"]?.s() ?: ""
        val email = extractItemId(itemId = item[emailPrimaryKey]?.s())
        val roles = mapRoles(attributeValues = item["roles"]?.l() ?: listOf())
        return item[primaryKey]?.s()?.let {
            User(
                id = extractItemId(itemId = it),
                email = email,
                verified = item["verified"]?.bool() ?: false,
                passwordHash = item["passwordHash"]?.s(),
                googleOAuthId = item["googleOAuthId"]?.s(),
                refreshToken = item[refreshTokenPrimaryKey]?.s(),
                userName = item["userName"]?.s(),
                roles = roles,
                created = if (dateAsString.isNotBlank()) Instant.parse(dateAsString) else Instant.now(),
            )
        }
    }

    private fun toUserResponseItem(item: Map<String, AttributeValue>): UserResponse? {
        val dateAsString = item["created"]?.s() ?: ""
        val email = extractItemId(itemId = item[emailPrimaryKey]?.s())
        val roles = mapRoles(attributeValues = item["roles"]?.l() ?: listOf())
        return item[primaryKey]?.s()?.let {
            UserResponse(
                id = extractItemId(itemId = it),
                userName = item["userName"]?.s(),
                email = email,
                verified = item["verified"]?.bool() ?: false,
                created = if (dateAsString.isNotBlank()) Instant.parse(dateAsString) else Instant.now(),
                roles = roles,
            )
        }
    }

    private fun toAuthUser(item: Map<String, AttributeValue>): AuthUser? {
        val dateAsString = item["created"]?.s() ?: ""
        val email = extractItemId(itemId = item[emailPrimaryKey]?.s())
        return item[primaryKey]?.s()?.let {
            AuthUser(
                id = extractItemId(itemId = it),
                created = if (dateAsString.isNotBlank()) Instant.parse(dateAsString) else Instant.now(),
                email = email,
                verified = item["verified"]?.bool() ?: false,
                verificationToken = item[verificationTokenPrimaryKey]?.s() ?: return null,
            )
        }
    }

    private fun mapRoles(attributeValues: List<AttributeValue>): List<Role> =
        attributeValues.map {
            Role.valueOf(it.s())
        }
}
