package com.markkewley.services

import com.markkewley.WheresTheFundsConfig
import com.markkewley.exception.InvalidRequestException
import com.markkewley.exception.NotFoundException
import com.markkewley.exception.WheresTheFundsException
import com.markkewley.models.CreateExpenseRequest
import com.markkewley.models.Expense
import com.markkewley.models.Fund
import com.markkewley.models.PaginatedResults
import com.markkewley.models.UpdateExpenseRequest
import com.markkewley.util.AppUUIDGenerator
import software.amazon.awssdk.services.dynamodb.DynamoDbClient
import software.amazon.awssdk.services.dynamodb.model.AttributeValue
import software.amazon.awssdk.services.dynamodb.model.BatchWriteItemRequest
import software.amazon.awssdk.services.dynamodb.model.PutRequest
import software.amazon.awssdk.services.dynamodb.model.UpdateItemRequest
import software.amazon.awssdk.services.dynamodb.model.WriteRequest
import java.time.Instant
import java.time.ZoneId
import java.time.temporal.ChronoUnit

/**
 * Service class for managing expenses in DynamoDB.
 *
 * Provides functionality for creating, updating, and querying expenses, as well as
 * associating them with specific funds and categories.
 *
 * @param dynamoDbClient The DynamoDB client used for database operations.
 * @param categoriesService The service used for category-related operations.
 * @param fundsService The service used for fund-related operations.
 */
class ExpensesService(
    dynamoDbClient: DynamoDbClient,
    private val categoriesService: CategoriesService,
    private val fundsService: FundsService,
) : DynamoService<Expense>(dynamoDbClient) {
    // Index and key names for querying DynamoDB, as configured in WheresTheFundsConfig
    private val userIdIndexName = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_ONE_NAME
    private val userIdPrimaryKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_ONE_PRIMARY_KEY
    private val userIdSortKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_ONE_SORT_KEY

    private val fundIdIndexName = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_TWO_NAME
    private val fundIdPrimaryKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_TWO_PRIMARY_KEY
    private val fundIdSortKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_TWO_SORT_KEY

    private val categoryIdIndexName = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_THREE_NAME
    private val categoryIdPrimaryKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_THREE_PRIMARY_KEY
    private val categoryIdSortKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_THREE_SORT_KEY

    /**
     * Retrieves all expenses for a given user, optionally filtering by date range.
     *
     * @param userId The user's identifier.
     * @param descending Specifies the order of the results; true for descending, false for ascending.
     * @param lastEvaluatedKey Optional pagination token for fetching the next set of results.
     * @param start Optional start date for filtering expenses.
     * @param end Optional end date for filtering expenses.
     * @return PaginatedResults of Expense objects.
     */
    fun getAllExpensesForUser(
        userId: String,
        descending: Boolean = true,
        lastEvaluatedKey: String? = null,
        start: String? = null,
        end: String? = null,
    ): PaginatedResults<Expense> {
        val lastEvaluatedKeyMap = lastEvaluatedKey?.let { mapOf(primaryKey to it) }
        return when {
            start != null || end != null ->
                queryRequestForBoundedExpenses(
                    userId = userId,
                    lastEvaluatedKey = lastEvaluatedKeyMap,
                    descending = descending,
                    start = start,
                    end = end,
                )

            else ->
                queryRequestForAllExpenses(
                    userId = userId,
                    lastEvaluatedKey = lastEvaluatedKeyMap,
                    descending = descending,
                )
        }
    }

    /**
     * Retrieves all expenses associated with a specific fund, optionally filtering by date range.
     *
     * @param fundId The fund's identifier.
     * @param start Start date for filtering expenses.
     * @param end End date for filtering expenses.
     * @param lastEvaluatedKey Optional pagination token for fetching the next set of results.
     * @param descending Specifies the order of the results; true for descending, false for ascending.
     * @return PaginatedResults of Expense objects.
     */
    fun getAllExpensesForFund(
        fundId: String,
        start: String,
        end: String,
        lastEvaluatedKey: String? = null,
        descending: Boolean = true,
    ): PaginatedResults<Expense> {
        val fundIdForDynamo =
            buildIndexItemId(entityName = expenseEntityName, indexEntityName = fundEntityName, indexItemId = fundId)
        return queryItems(
            keyConditionExpression = "#pk = :fundId and #sk between :start and :end",
            expressionAttributeNames =
                mapOf(
                    "#pk" to fundIdPrimaryKey,
                    "#sk" to fundIdSortKey,
                ),
            expressionAttributeValues =
                mapOf(
                    ":fundId" to AttributeValue.builder().s(fundIdForDynamo).build(),
                    ":start" to AttributeValue.builder().s(start).build(),
                    ":end" to AttributeValue.builder().s(end).build(),
                ),
            indexName = fundIdIndexName,
            lastEvaluatedKey = lastEvaluatedKey?.let { mapOf(primaryKey to it) },
            descending = descending,
        )
    }

    /**
     * Retrieves all expenses associated with a specific category, optionally filtering by date range.
     *
     * @param categoryId The category's identifier.
     * @param start Start date for filtering expenses.
     * @param end End date for filtering expenses.
     * @param lastEvaluatedKey Optional pagination token for fetching the next set of results.
     * @param descending Specifies the order of the results; true for descending, false for ascending.
     * @return PaginatedResults of Expense objects.
     */
    fun getAllExpensesForCategory(
        categoryId: String,
        start: String,
        end: String,
        lastEvaluatedKey: Map<String, String>? = null,
        descending: Boolean = true,
    ) = queryItems<Expense>(
        keyConditionExpression = "#pk = :categoryId and #sk between :start and :end",
        expressionAttributeNames =
            mapOf(
                "#pk" to categoryIdPrimaryKey,
                "#sk" to categoryIdSortKey,
            ),
        expressionAttributeValues =
            mapOf(
                ":categoryId" to
                    AttributeValue.builder()
                        .s(
                            buildIndexItemId(
                                entityName = expenseEntityName,
                                indexEntityName = categoryEntityName,
                                indexItemId = categoryId,
                            ),
                        ).build(),
                ":start" to AttributeValue.builder().s(start).build(),
                ":end" to AttributeValue.builder().s(end).build(),
            ),
        lastEvaluatedKey = lastEvaluatedKey,
        indexName = categoryIdIndexName,
        descending = descending,
    )

    fun getExpenseById(
        userId: String,
        expenseId: String,
    ): Expense =
        getItemById(
            userId = userId,
            itemId = expenseId,
            entityName = expenseEntityName,
        ) ?: throw NotFoundException("Expense $expenseId not found")

    /**
     * Creates a new expense in the database.
     *
     * @param createExpenseRequest The request object containing the details of the expense to create.
     * @param userId The identifier of the user creating the expense.
     * @return The created Expense object.
     */
    fun createExpense(
        createExpenseRequest: CreateExpenseRequest,
        userId: String,
    ): Expense {
        val itemId = AppUUIDGenerator.generateUUID(createExpenseRequest.date).toString()

        val fund = determineFundFromCategory(userId = userId, categoryId = createExpenseRequest.categoryId)
        val categoryIdForDynamo =
            buildIndexItemId(
                entityName = expenseEntityName,
                indexEntityName = categoryEntityName,
                indexItemId = createExpenseRequest.categoryId,
            )
        val itemValues =
            createItemRequest(
                userId = userId,
                itemId = itemId,
                categoryIdForDynamo = categoryIdForDynamo,
                createExpenseRequest = createExpenseRequest,
                fundId = fund?.id,
            )

        return createItem(
            itemId = itemId,
            userId = userId,
            entityName = expenseEntityName,
            itemValues = itemValues,
        )
    }

    /**
     * Updates an existing expense with new details.
     *
     * If the [UpdateExpenseRequest.date] is changed we delete the existing Expense and create a new one.
     *
     * @param userId The identifier of the user who owns the expense.
     * @param expenseId The identifier of the expense to update.
     * @param updateExpenseRequest The request object containing the updated details of the expense.
     * @return The identifier of the updated expense.
     */
    fun updateExpense(
        userId: String,
        expenseId: String,
        updateExpenseRequest: UpdateExpenseRequest,
    ): String {
        val expenseIdForDynamo = buildItemId(entityName = expenseEntityName, itemId = expenseId)
        val userIdForDynamo = buildItemId(entityName = userEntityName, itemId = userId)

        val key =
            mapOf(
                primaryKey to AttributeValue.builder().s(userIdForDynamo).build(),
                sortKey to AttributeValue.builder().s(expenseIdForDynamo).build(),
            )

        val existingExpense =
            getItemById<Expense>(
                userId = userId,
                itemId = expenseId,
                entityName = expenseEntityName,
            ) ?: throw NotFoundException("Expense not found $expenseId")

        if (!isSameDay(instantOne = updateExpenseRequest.date, instantTwo = existingExpense.date)) {
            println("Deleting expense: $expenseId as we are updating the date field")
            // To preserve the sort we must delete the Expense and recreate if we wish for optimal performance
            try {
                deleteExpense(userId = userId, expenseId = expenseId)
                println("Successfully deleted expense $expenseId")
                return createExpense(
                    createExpenseRequest =
                        CreateExpenseRequest(
                            amount = updateExpenseRequest.amount,
                            description = updateExpenseRequest.description,
                            categoryId = updateExpenseRequest.categoryId,
                            date = updateExpenseRequest.date,
                        ),
                    userId = userId,
                ).id
            } catch (e: Exception) {
                println("Error creating new expense")
                e.printStackTrace()
            }
        } else {
            val categoryId =
                buildIndexItemId(
                    entityName = expenseEntityName,
                    indexEntityName = categoryEntityName,
                    indexItemId = updateExpenseRequest.categoryId,
                )

            // Sort keys remain same for Category/Fund as date has not changed
            var updateExpression = "SET #a = :amount, #d = :description, #cpk = :category, #csk = :category"
            val expressionAttributeNames =
                mutableMapOf(
                    "#a" to "amount",
                    "#d" to "description",
                    "#cpk" to categoryIdPrimaryKey,
                    "#csk" to categoryIdSortKey,
                )
            val expressionAttributeValues =
                mutableMapOf<String, AttributeValue>(
                    ":amount" to AttributeValue.builder().n(updateExpenseRequest.amount.toString()).build(),
                    ":description" to AttributeValue.builder().s(updateExpenseRequest.description).build(),
                    ":category" to AttributeValue.builder().s(categoryId).build(),
                )

            // Check for existing fund (also verifies if the category exists)
            val fund = determineFundFromCategory(userId = userId, categoryId = updateExpenseRequest.categoryId)
            if (fund != null) {
                updateExpression += ", #fpk = :fundId"
                expressionAttributeNames["#fpk"] = fundIdPrimaryKey
                expressionAttributeValues[":fundId"] =
                    AttributeValue.builder().s(
                        buildIndexItemId(
                            entityName = expenseEntityName,
                            indexEntityName = fundEntityName,
                            indexItemId = fund.id,
                        ),
                    ).build()
            }

            val updateItemRequest =
                UpdateItemRequest.builder()
                    .tableName(tableName)
                    .key(key)
                    .updateExpression(updateExpression)
                    .expressionAttributeNames(expressionAttributeNames)
                    .expressionAttributeValues(expressionAttributeValues)
                    .build()

            try {
                dynamoDbClient.updateItem(updateItemRequest)
                println("Successfully updated expense $expenseId")
                return expenseId
            } catch (e: Exception) {
                println("Unable to update expense $expenseId")
                e.printStackTrace()
            }
        }
        throw WheresTheFundsException("Unable to update expense $expenseId")
    }

    /**
     * Will remove en Expense record from Dynamo.
     *
     * @return T if successful, F otherwise
     */
    fun deleteExpense(
        userId: String,
        expenseId: String,
    ) = deleteItem(userId = userId, itemId = expenseId, entityName = expenseEntityName)

    /**
     * This will update all Expenses from one year ago to one year from now that belong to the [categoryId]. It will
     * add the [fundId] to all of them.
     *
     * This will do write requests in batches of 25 to reduce throughput.
     *
     * @param userId - UUID representing a User
     * @parm fundId - UUID representing a Fund
     * @param categoryId - UUID representing a Category
     */
    fun setFundIdForAllRecordsByCategory(
        userId: String,
        fundId: String,
        categoryId: String,
    ) {
        val categoryIdForDynamo =
            buildIndexItemId(
                entityName = expenseEntityName,
                indexEntityName = categoryEntityName,
                indexItemId = categoryId,
            )
        // Only update the past year into next year
        val oneYearAgo = Instant.now().minus(365, ChronoUnit.DAYS).toString()
        val oneYearFromNow = Instant.now().plus(365, ChronoUnit.DAYS).toString()

        var response = getAllExpensesForCategory(categoryId = categoryId, start = oneYearAgo, end = oneYearFromNow)
        val expenses: MutableList<Expense> = response.items.toMutableList()
        while (response.lastEvaluatedKey.isNotEmpty()) {
            response =
                getAllExpensesForCategory(
                    categoryId = categoryId,
                    start = oneYearAgo,
                    end = oneYearFromNow,
                    lastEvaluatedKey = response.lastEvaluatedKey,
                )
            expenses += response.items
        }

        // Batch the updates
        val batches = expenses.chunked(25) // DynamoDB batch limit

        println("Processing ${expenses.size} expenses in batches of 25...")
        batches.forEach { batch ->
            println("Updating expenses: ${batch.map { it.id }}")
            val writeRequests =
                batch.map { item ->

                    val updatedItem =
                        createItemRequest(
                            userId = userId,
                            itemId = item.id,
                            categoryIdForDynamo = categoryIdForDynamo,
                            createExpenseRequest =
                                CreateExpenseRequest(
                                    amount = item.amount,
                                    description = item.description,
                                    categoryId = item.categoryId,
                                    date = item.date,
                                ),
                            fundId = fundId,
                        )

                    WriteRequest.builder()
                        .putRequest(PutRequest.builder().item(updatedItem).build())
                        .build()
                }

            val batchWriteItemRequest =
                BatchWriteItemRequest.builder()
                    .requestItems(mapOf(tableName to writeRequests))
                    .build()

            dynamoDbClient.batchWriteItem(batchWriteItemRequest)
            println("Successfully batched update")
        }
    }

    /**
     * Queries all expenses for a given user without date bounds.
     *
     * This function queries the DynamoDB table for all expenses associated with a specific user.
     * The query can be ordered in ascending or descending order based on the expense creation date.
     *
     * @param userId The identifier of the user whose expenses are to be queried.
     * @param lastEvaluatedKey A map containing the last evaluated key for pagination purposes. Null if querying from the beginning.
     * @param descending Boolean flag indicating whether the results should be ordered in descending order (true) or ascending order (false).
     * @return PaginatedResults<Expense> containing the queried expenses and information for further pagination.
     */
    private fun queryRequestForAllExpenses(
        userId: String,
        lastEvaluatedKey: Map<String, String>?,
        descending: Boolean,
    ) = queryItems<Expense>(
        keyConditionExpression = "#pk = :userId and begins_with(#sk, :sortKeyPrefix)",
        expressionAttributeNames =
            mapOf(
                "#pk" to primaryKey,
                "#sk" to sortKey,
            ),
        expressionAttributeValues =
            mapOf(
                ":userId" to AttributeValue.builder().s(buildItemId(entityName = userEntityName, itemId = userId)).build(),
                ":sortKeyPrefix" to AttributeValue.builder().s("$expenseEntityName#").build(),
            ),
        lastEvaluatedKey = lastEvaluatedKey,
        descending = descending,
    )

    /**
     * Queries expenses for a given user within a specified date range.
     *
     * This function allows for querying expenses that fall within a specific date range. It can be used to
     * retrieve expenses for reporting or analysis purposes. The results can be ordered by the expense date
     * in either ascending or descending order. The function supports pagination through the lastEvaluatedKey parameter.
     *
     * @param userId The identifier of the user whose expenses are to be queried.
     * @param descending Boolean flag indicating the order of the results; true for descending, false for ascending.
     * @param lastEvaluatedKey Optional parameter for pagination. Specifies the last evaluated key from a previous query.
     * @param start The start date of the range for which expenses are queried. Null to ignore the start date.
     * @param end The end date of the range for which expenses are queried. Null to ignore the end date.
     * @return PaginatedResults<Expense> containing the queried expenses within the specified range and pagination information.
     * @throws InvalidRequestException If both start and end parameters are null, indicating a bounded query is required.
     */
    private fun queryRequestForBoundedExpenses(
        userId: String,
        descending: Boolean,
        lastEvaluatedKey: Map<String, String>?,
        start: String? = null,
        end: String? = null,
    ): PaginatedResults<Expense> {
        val expressionAttributeNames = mutableMapOf("#pk" to userIdPrimaryKey, "#sk" to userIdSortKey)
        val expressionAttributeValues =
            mutableMapOf(
                ":userId" to
                    AttributeValue.builder().s(
                        buildIndexItemId(
                            entityName = expenseEntityName,
                            indexEntityName = userEntityName,
                            indexItemId = userId,
                        ),
                    )
                        .build(),
            )
        var keyConditionExpression = "#pk = :userId"

        when {
            start != null && end != null -> {
                expressionAttributeValues[":start"] = AttributeValue.builder().s(start).build()
                expressionAttributeValues[":end"] = AttributeValue.builder().s(end).build()
                keyConditionExpression += " and #sk between :start and :end"
            }

            start != null -> {
                expressionAttributeValues[":start"] = AttributeValue.builder().s(start).build()
                keyConditionExpression += " and #sk >= :start"
            }

            end != null -> {
                expressionAttributeValues[":end"] = AttributeValue.builder().s(end).build()
                keyConditionExpression += " and #sk <= :end"
            }

            else -> throw InvalidRequestException("start/end are required")
        }

        return queryItems(
            keyConditionExpression = keyConditionExpression,
            expressionAttributeNames = expressionAttributeNames,
            expressionAttributeValues = expressionAttributeValues,
            lastEvaluatedKey = lastEvaluatedKey,
            descending = descending,
            indexName = userIdIndexName,
        )
    }

    /**
     * Checks for an existing fund for the provided [categoryId].
     *
     * It first checks if there is an existing category, if not an error is thrown
     *
     * @param userId - User identifier (UUID)
     * @param categoryId - Category identifier (UUID)
     * @return [Fund] object if it exists
     * @throws InvalidRequestException - if the [userId], [categoryId] composite key does not exist
     */
    private fun determineFundFromCategory(
        userId: String,
        categoryId: String,
    ): Fund? {
        categoriesService.getCategoryById(userId = userId, categoryId = categoryId)
            ?: throw InvalidRequestException("Category does not exist")

        return fundsService.getFundByCategory(categoryId = categoryId)
    }

    /**
     * Converts a DynamoDB item map to a [Expense] object.
     *
     * This method is an implementation of the abstract method defined in DynamoService. It extracts attribute values
     * from a DynamoDB item map and constructs a [Expense] object.
     *
     * @param item The map of attribute names to [AttributeValue] objects representing an expense item in DynamoDB.
     * @return A [Expense] object constructed from the item map, or null if required attributes are missing.
     */
    override fun toItem(item: Map<String, AttributeValue>): Expense? {
        val dateAsString = item["date"]?.s() ?: ""
        val expenseId = extractItemId(itemId = item[sortKey]?.s())
        val userId = extractItemId(itemId = item[primaryKey]?.s())
        val categoryId = extractItemId(itemId = item[categoryIdPrimaryKey]?.s())
        val fundId = extractItemId(itemId = item[fundIdPrimaryKey]?.s())
        return Expense(
            id = expenseId.ifEmpty { return null },
            userId = userId.ifEmpty { return null },
            amount = item["amount"]?.n()?.toDouble() ?: 0.0,
            description = item["description"]?.s() ?: "",
            categoryId = categoryId,
            date = if (dateAsString.trim() != "") Instant.parse(dateAsString) else Instant.now(),
            fundId = fundId,
        )
    }

    /**
     * Constructs a map of attribute values for creating a new [Expense] item in DynamoDB.
     *
     * @param userId The user identifier.
     * @param itemId ItemId for [Expense.id]
     * @param categoryIdForDynamo The DynamoDB item identifier for a category
     * @param createExpenseRequest [CreateExpenseRequest] create expense data class
     * @param fundId An optional fund id for the [Expense]
     * @return A map representing the DynamoDB item for the new category.
     */
    private fun createItemRequest(
        userId: String,
        itemId: String,
        categoryIdForDynamo: String,
        createExpenseRequest: CreateExpenseRequest,
        fundId: String? = null,
    ) = mutableMapOf<String, AttributeValue>(
        primaryKey to AttributeValue.builder().s(buildItemId(entityName = userEntityName, itemId = userId)).build(),
        sortKey to AttributeValue.builder().s(buildItemId(entityName = expenseEntityName, itemId = itemId)).build(),
        "amount" to AttributeValue.builder().n(createExpenseRequest.amount.toString()).build(),
        "description" to AttributeValue.builder().s(createExpenseRequest.description).build(),
        "date" to AttributeValue.builder().s(createExpenseRequest.date.toString()).build(),
        userIdPrimaryKey to
            AttributeValue.builder().s(
                buildIndexItemId(
                    entityName = expenseEntityName,
                    indexEntityName = userEntityName,
                    indexItemId = userId,
                ),
            ).build(),
        userIdSortKey to AttributeValue.builder().s(createExpenseRequest.date.toString()).build(),
        categoryIdPrimaryKey to AttributeValue.builder().s(categoryIdForDynamo).build(),
        categoryIdSortKey to AttributeValue.builder().s(createExpenseRequest.date.toString()).build(),
        entityTypePrimaryKey to AttributeValue.builder().s(expenseEntityName).build(),
        entityTypeSortKey to AttributeValue.builder().s(createExpenseRequest.date.toString()).build(),
    ).apply {
        if (fundId != null) {
            val fundItemId =
                buildIndexItemId(entityName = expenseEntityName, indexEntityName = fundEntityName, indexItemId = fundId)
            this[fundIdPrimaryKey] = AttributeValue.builder().s(fundItemId).build()
            this[fundIdSortKey] = AttributeValue.builder().s(createExpenseRequest.date.toString()).build()
        }
    }

    private fun isSameDay(
        instantOne: Instant,
        instantTwo: Instant,
    ): Boolean {
        val zoneId = ZoneId.systemDefault()
        return instantOne.atZone(zoneId).toLocalDate().equals(instantTwo.atZone(zoneId).toLocalDate())
    }
}
