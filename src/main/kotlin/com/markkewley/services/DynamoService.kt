package com.markkewley.services

import com.markkewley.WheresTheFundsConfig
import com.markkewley.exception.WheresTheFundsException
import com.markkewley.models.PaginatedResults
import software.amazon.awssdk.services.dynamodb.DynamoDbClient
import software.amazon.awssdk.services.dynamodb.model.AttributeValue
import software.amazon.awssdk.services.dynamodb.model.DeleteItemRequest
import software.amazon.awssdk.services.dynamodb.model.PutItemRequest
import software.amazon.awssdk.services.dynamodb.model.QueryRequest
import software.amazon.awssdk.services.dynamodb.model.UpdateItemRequest

/**
 * Abstract base class for services interacting with DynamoDB.
 *
 * Provides utility methods for querying and manipulating items in a DynamoDB table
 * specific to the WheresTheFunds application.
 *
 * @property dynamoDbClient The DynamoDB client used for database operations.
 * @param T The type of the item to be handled by the service.
 */
abstract class DynamoService<T>(val dynamoDbClient: DynamoDbClient) {
    val tableName: String = WheresTheFundsConfig.primaryTableName

    val primaryKey: String = WheresTheFundsConfig.PRIMARY_KEY
    val sortKey: String = WheresTheFundsConfig.SORT_KEY

    val entityTypeIndexName: String = WheresTheFundsConfig.ENTITY_TYPE_INDEX_NAME
    val entityTypePrimaryKey: String = WheresTheFundsConfig.ENTITY_TYPE_PRIMARY_KEY
    val entityTypeSortKey: String = WheresTheFundsConfig.ENTITY_TYPE_SORT_KEY

    val categoryEntityName = WheresTheFundsConfig.categoryEntityName
    val expenseEntityName = WheresTheFundsConfig.expenseEntityName
    val expenseEventEntityName = WheresTheFundsConfig.expenseEventEntityName
    val fundEntityName = WheresTheFundsConfig.fundEntityName
    val userEntityName = WheresTheFundsConfig.userEntityName

    /**
     * Abstract method to convert a DynamoDB item map to an instance of [T].
     *
     * Implementing classes must provide a concrete implementation to convert a DynamoDB item
     * to the appropriate type [T].
     *
     * @param item The DynamoDB item map.
     * @return An instance of [T] created from the item map, or null if the conversion is not possible.
     */
    abstract fun toItem(item: Map<String, AttributeValue>): T?

    /**
     * Extracts the item ID from a DynamoDB attribute value, formatted as <entityName>#<itemId>.
     *
     * @param itemId The item ID attribute value.
     * @return The extracted item ID or an empty string if the ID is not present.
     */
    fun extractItemId(itemId: String?) = itemId?.split("#")?.last() ?: ""

    /**
     * Constructs an item ID for storing in DynamoDB, formatted as <entityName>#<itemId>.
     *
     * @param entityName The name of the entity.
     * @param itemId The unique identifier of the item.
     * @return The constructed DynamoDB item ID.
     */
    fun buildItemId(
        entityName: String,
        itemId: String,
    ) = "$entityName#$itemId"

    /**
     * Constructs an item ID for storing a Global Secondary Index identifier in DynamoDB,
     * formatted as <entityName>#<indexEntityName>#<itemId>.
     *
     * @param entityName The name of the entity.
     * @param indexEntityName The index's entity name (e.g. category)
     * @param indexItemId The unique identifier of the index's item (e.g. id for category)
     * @return The constructed DynamoDB GSI ID.
     */
    fun buildIndexItemId(
        entityName: String,
        indexEntityName: String,
        indexItemId: String,
    ) = "$entityName#$indexEntityName#$indexItemId"

    /**
     * Queries items from DynamoDB based on the provided index name and key condition expression.
     *
     * @param keyConditionExpression The key condition expression for the query.
     * @param expressionAttributeNames A mapping of expression attribute names to keys.
     * @param expressionAttributeValues A mapping of expression attribute values.
     * @param toItemFn A function to convert DynamoDB items to instances of [T]. Defaults to [toItem].
     * @param indexName The name of the index to query against (optional).
     * @param lastEvaluatedKey The pagination token for fetching the next set of results (optional).
     * @param descending Whether to sort the results in descending order. Defaults to false
     * @return A [PaginatedResults] containing the query results and pagination information.
     */
    fun <E> queryItems(
        keyConditionExpression: String,
        expressionAttributeNames: Map<String, String>,
        expressionAttributeValues: Map<String, AttributeValue>,
        toItemFn: (Map<String, AttributeValue>) -> E? = { toItem(it) as E? },
        indexName: String? = null,
        lastEvaluatedKey: Map<String, String>? = null,
        descending: Boolean = false,
    ): PaginatedResults<E> {
        val queryRequest =
            QueryRequest
                .builder()
                .tableName(tableName)
                .keyConditionExpression(keyConditionExpression)
                .expressionAttributeNames(expressionAttributeNames)
                .expressionAttributeValues(expressionAttributeValues)

        indexName?.let {
            queryRequest.indexName(it)
        }

        queryRequest.scanIndexForward(!descending)
        lastEvaluatedKey?.let {
            queryRequest.exclusiveStartKey(it.mapValues { value -> AttributeValue.builder().s(value.value).build() })
        }

        val response = dynamoDbClient.query(queryRequest.build())
        val items = response.items().mapNotNull { toItemFn(it) }
        val lastKey = response.lastEvaluatedKey()
        return PaginatedResults(
            items = items,
            lastEvaluatedKey = lastKey.mapValues { it.value.s() },
        )
    }

    /**
     * Queries for a single item from DynamoDB based on the provided key condition.
     *
     * @param keyConditionExpression The key condition expression for the query.
     * @param expressionAttributeNames A mapping of expression attribute names to keys.
     * @param expressionAttributeValues A mapping of expression attribute values.
     * @param toItemFn A function to convert DynamoDB items to instances of [T].
     * @param indexName The name of the index to query against (optional).
     * @param lastEvaluatedKey The pagination token for fetching the next set of results (optional).
     * @param descending Whether to sort the results in descending order.
     * @return A pair containing the first matched item (if any) and pagination information.
     */
    fun <E> querySingleItem(
        keyConditionExpression: String,
        expressionAttributeNames: Map<String, String>,
        expressionAttributeValues: Map<String, AttributeValue>,
        toItemFn: (Map<String, AttributeValue>) -> E? = { toItem(it) as E? },
        indexName: String? = null,
        lastEvaluatedKey: Map<String, String>? = null,
        descending: Boolean = false,
    ): Pair<E?, Map<String, String>?> {
        val queryRequest =
            QueryRequest
                .builder()
                .tableName(tableName)
                .keyConditionExpression(keyConditionExpression)
                .expressionAttributeNames(expressionAttributeNames)
                .expressionAttributeValues(expressionAttributeValues)

        indexName?.let {
            queryRequest.indexName(it)
        }

        queryRequest.scanIndexForward(!descending)
        lastEvaluatedKey?.let {
            queryRequest.exclusiveStartKey(it.mapValues { value -> AttributeValue.builder().s(value.value).build() })
        }

        val response = dynamoDbClient.query(queryRequest.build())
        val lastKey = response.lastEvaluatedKey()
        return Pair(
            response.items().map { toItemFn(it) }.firstOrNull(),
            lastKey.mapValues { it.value.s() },
        )
    }

    /**
     * Retrieves an item by its ID and specific entity name.
     *
     * @param userId The user ID associated with the item.
     * @param itemId The unique identifier of the item.
     * @param entityName The entity name of the item.
     * @param toItemFn A function to convert DynamoDB items to instances of [T].
     * @return An instance of [T] representing the item, or null if not found.
     */
    fun <E> getItemById(
        userId: String,
        itemId: String,
        entityName: String,
        toItemFn: (Map<String, AttributeValue>) -> E? = { toItem(it) as E? },
    ): E? {
        val dynamoItemId = buildItemId(entityName = entityName, itemId = itemId)
        val userItemId = buildItemId(entityName = userEntityName, itemId = userId)
        val queryRequest =
            QueryRequest
                .builder()
                .tableName(tableName)
                .keyConditionExpression("#pk = :userId and #sk = :itemId")
                .expressionAttributeNames(
                    mapOf(
                        "#pk" to primaryKey,
                        "#sk" to sortKey,
                    ),
                )
                .expressionAttributeValues(
                    mapOf(
                        ":userId" to AttributeValue.builder().s(userItemId).build(),
                        ":itemId" to AttributeValue.builder().s(dynamoItemId).build(),
                    ),
                )
                .build()

        val response = dynamoDbClient.query(queryRequest)
        return response.items().map { toItemFn(it) }.firstOrNull()
    }

    /**
     * Creates a new item in DynamoDB and returns the created item.
     *
     * @param userId The user ID associated with the item.
     * @param itemId The unique identifier for the new item.
     * @param itemValues A map of attribute values for the new item.
     * @param entityName The entity name of the item.
     * @param conditionExpression Expressions that can add uniqueness (e.g. attribute_not_exists(fundId)
     * @param toItemFn A function to convert DynamoDB items to instances of [T].
     * @return The created item as an instance of [T].
     * @throws WheresTheFundsException if the item cannot be created or retrieved after creation.
     */
    fun <E> createItem(
        userId: String,
        itemId: String,
        itemValues: Map<String, AttributeValue>,
        entityName: String,
        conditionExpression: String? = null,
        toItemFn: (Map<String, AttributeValue>) -> E? = { toItem(it) as E? },
    ): E {
        val putItemRequest =
            PutItemRequest.builder()
                .tableName(tableName)
                .item(itemValues)

        conditionExpression.let {
            putItemRequest.conditionExpression(it)
        }

        try {
            dynamoDbClient.putItem(putItemRequest.build())
            println("Successfully created [$entityName]: [$itemId] - for user [$userId]")
            return getItemById(userId = userId, itemId = itemId, entityName = entityName, toItemFn = { toItemFn(it) })
                ?: throw WheresTheFundsException("Unknown error occurred retrieving newly created [$entityName]")
        } catch (e: Exception) {
            throw WheresTheFundsException("Unable to create [$entityName]: [$itemId] for user [$userId] - ${e.localizedMessage}")
        }
    }

    /**
     * Updates a new item in DynamoDB and True if successful, false otherwise
     *
     * @param userId The user ID associated with the item.
     * @param itemId The unique identifier for the new item.
     * @param updateExpression The update expression
     * @param entityName The entity name of the item.
     * @param expressionAttributeNames A mapping of expression attribute names to keys.
     * @param expressionAttributeValues A mapping of expression attribute values.
     * @return True if successful, False otherwise.
     */
    fun updateItem(
        userId: String,
        itemId: String,
        entityName: String,
        updateExpression: String,
        expressionAttributeNames: Map<String, String>,
        expressionAttributeValues: Map<String, AttributeValue>,
    ): Boolean {
        val updateItemRequest =
            UpdateItemRequest.builder()
                .tableName(tableName)
                .key(
                    mapOf(
                        primaryKey to AttributeValue.builder().s(buildItemId(entityName = userEntityName, itemId = userId)).build(),
                        sortKey to AttributeValue.builder().s(buildItemId(entityName = entityName, itemId = itemId)).build(),
                    ),
                )
                .updateExpression(updateExpression)
                .expressionAttributeNames(expressionAttributeNames)
                .expressionAttributeValues(expressionAttributeValues)
                .build()

        try {
            dynamoDbClient.updateItem(updateItemRequest)
            println("Successfully updated item[$entityName] - userId[$userId], itemId[$itemId] ")
            return true
        } catch (e: Exception) {
            println("Unable to updated item[$entityName] - userId[$userId], itemId[$itemId] - ${e.localizedMessage} ")
        }
        return false
    }

    /**
     * Deletes an item from DynamoDB
     *
     * @param userId The user ID associated with the item.
     * @param itemId The unique identifier for the new item.
     * @param entityName The entity name of the item.
     * @return [Boolean] T if success, F otherwise
     */
    fun deleteItem(
        userId: String,
        itemId: String,
        entityName: String,
    ): Boolean {
        val deleteItemRequest =
            DeleteItemRequest.builder()
                .tableName(tableName)
                .key(
                    mapOf(
                        primaryKey to AttributeValue.builder().s(buildItemId(entityName = userEntityName, itemId = userId)).build(),
                        sortKey to AttributeValue.builder().s(buildItemId(entityName = entityName, itemId = itemId)).build(),
                    ),
                )
                .build()

        try {
            dynamoDbClient.deleteItem(deleteItemRequest)
            println("Deleted item[$entityName] - userId[$userId], itemId[$itemId]")
            return true
        } catch (e: Exception) {
            println("Unable to delete item[$entityName] - userId[$userId], itemId[$itemId] - ${e.localizedMessage}")
        }
        return false
    }
}
