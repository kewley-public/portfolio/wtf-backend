package com.markkewley.services

import com.markkewley.WheresTheFundsConfig
import com.markkewley.exception.InvalidRequestException
import com.markkewley.models.CreateExpenseEventRequest
import com.markkewley.models.ExpenseEvent
import com.markkewley.util.AppUUIDGenerator
import software.amazon.awssdk.services.dynamodb.DynamoDbClient
import software.amazon.awssdk.services.dynamodb.model.AttributeValue
import java.time.Instant

class ExpenseEventsService(dynamoDbClient: DynamoDbClient, private val categoriesService: CategoriesService) :
    DynamoService<ExpenseEvent>(dynamoDbClient) {
    private val categoryIdIndexName = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_ONE_NAME
    private val categoryIdPrimaryKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_ONE_PRIMARY_KEY
    private val categoryIdSortKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_ONE_SORT_KEY

    private val userIdIndexName = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_TWO_NAME
    private val userIdPrimaryKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_TWO_PRIMARY_KEY
    private val userIdSortKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_TWO_SORT_KEY

    /**
     * Retrieves all [ExpenseEvent]s for a given user
     *
     * @param userId The user's identifier.
     * @param start start date for filtering expenses.
     * @param end end date for filtering expenses.
     * @param descending Specifies the order of the results; true for descending, false for ascending.
     * @param lastEvaluatedKey Optional pagination token for fetching the next set of results.
     * @return PaginatedResults of [ExpenseEvent] objects.
     */
    fun getAllEventsForUser(
        userId: String,
        start: String,
        end: String,
        descending: Boolean = true,
        lastEvaluatedKey: String? = null,
    ) = queryItems<ExpenseEvent>(
        keyConditionExpression = "#pk = :userId and #sk between :start and :end",
        expressionAttributeNames =
            mapOf(
                "#pk" to userIdPrimaryKey,
                "#sk" to userIdSortKey,
            ),
        expressionAttributeValues =
            mapOf(
                ":userId" to
                    AttributeValue.builder().s(
                        buildIndexItemId(
                            entityName = expenseEventEntityName,
                            indexEntityName = userEntityName,
                            indexItemId = userId,
                        ),
                    ).build(),
                ":start" to AttributeValue.builder().s(start).build(),
                ":end" to AttributeValue.builder().s(end).build(),
            ),
        lastEvaluatedKey = lastEvaluatedKey?.let { mapOf(primaryKey to it) },
        descending = descending,
        indexName = userIdIndexName,
    )

    /**
     * Creates a [ExpenseEvent] record.
     *
     * @param userId The user's identifier.
     * @param createExpenseEventRequest [CreateExpenseEventRequest] request that holds onto all the data to
     *  create an [ExpenseEvent]
     * @return [ExpenseEvent] if successful
     */
    fun createEvent(
        userId: String,
        createExpenseEventRequest: CreateExpenseEventRequest,
    ): ExpenseEvent {
        categoriesService.getCategoryById(userId = userId, categoryId = createExpenseEventRequest.categoryId)
            ?: throw InvalidRequestException("Invalid category")

        val itemId = AppUUIDGenerator.generateUUID(createExpenseEventRequest.date).toString()
        val itemValues =
            buildCreationMap(
                userId = userId,
                createExpenseEventRequest = createExpenseEventRequest,
                itemId = itemId,
            )

        return createItem(
            userId = userId,
            itemId = itemId,
            itemValues = itemValues,
            entityName = expenseEventEntityName,
        )
    }

    /**
     * Converts a DynamoDB item map to a [ExpenseEvent] object.
     *
     * This method is an implementation of the abstract method defined in DynamoService. It extracts attribute values
     * from a DynamoDB item map and constructs a [ExpenseEvent] object.
     *
     * @param item The map of attribute names to [AttributeValue] objects representing an expense item in DynamoDB.
     * @return A [ExpenseEvent] object constructed from the item map, or null if required attributes are missing.
     */
    override fun toItem(item: Map<String, AttributeValue>): ExpenseEvent? {
        val dateAsString = item["date"]?.s() ?: ""
        val eventId = extractItemId(itemId = item[sortKey]?.s())
        val categoryId = extractItemId(itemId = item[categoryIdPrimaryKey]?.s())
        return ExpenseEvent(
            id = eventId.ifEmpty { return null },
            amount = item["amount"]?.n()?.toDouble() ?: 0.0,
            description = item["description"]?.s() ?: "",
            categoryId = categoryId,
            date = if (dateAsString.trim() != "") Instant.parse(dateAsString) else Instant.now(),
        )
    }

    private fun buildCreationMap(
        userId: String,
        createExpenseEventRequest: CreateExpenseEventRequest,
        itemId: String? = null,
    ): Map<String, AttributeValue> {
        val itemIdForDynamo =
            buildItemId(
                entityName = expenseEventEntityName,
                itemId = itemId ?: AppUUIDGenerator.generateUUID(createExpenseEventRequest.date).toString(),
            )
        val userIndexForDynamo =
            buildIndexItemId(
                entityName = expenseEventEntityName,
                indexEntityName = userEntityName,
                indexItemId = userId,
            )
        val categoryIdForDynamo =
            buildIndexItemId(
                entityName = expenseEventEntityName,
                indexEntityName = categoryEntityName,
                indexItemId = createExpenseEventRequest.categoryId,
            )
        return mapOf(
            primaryKey to AttributeValue.builder().s(buildItemId(entityName = userEntityName, itemId = userId)).build(),
            sortKey to AttributeValue.builder().s(itemIdForDynamo).build(),
            "amount" to AttributeValue.builder().n(createExpenseEventRequest.amount.toString()).build(),
            "description" to AttributeValue.builder().s(createExpenseEventRequest.description).build(),
            "date" to AttributeValue.builder().s(createExpenseEventRequest.date.toString()).build(),
            categoryIdPrimaryKey to AttributeValue.builder().s(categoryIdForDynamo).build(),
            categoryIdSortKey to AttributeValue.builder().s(categoryIdForDynamo).build(),
            userIdPrimaryKey to AttributeValue.builder().s(userIndexForDynamo).build(),
            userIdSortKey to AttributeValue.builder().s(createExpenseEventRequest.date.toString()).build(),
        )
    }
}
