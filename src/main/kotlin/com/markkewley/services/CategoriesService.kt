package com.markkewley.services

import com.markkewley.WheresTheFundsConfig
import com.markkewley.exception.WheresTheFundsException
import com.markkewley.models.Category
import com.markkewley.models.ColorCodeCategory
import com.markkewley.models.CreateCategoryRequest
import com.markkewley.models.InitialCreateCategory
import com.markkewley.util.AppUUIDGenerator
import software.amazon.awssdk.services.dynamodb.DynamoDbClient
import software.amazon.awssdk.services.dynamodb.model.AttributeValue
import software.amazon.awssdk.services.dynamodb.model.BatchWriteItemRequest
import software.amazon.awssdk.services.dynamodb.model.PutRequest
import software.amazon.awssdk.services.dynamodb.model.WriteRequest

/**
 * Service for managing category-related operations in DynamoDB.
 *
 * @param dynamoDbClient The DynamoDB client used for database operations.
 */
class CategoriesService(dynamoDbClient: DynamoDbClient) : DynamoService<Category>(dynamoDbClient) {
    private val fundIdIndexName = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_ONE_NAME
    private val fundIdPrimaryKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_ONE_PRIMARY_KEY
    private val fundIdSortKey = WheresTheFundsConfig.GLOBAL_SECONDARY_INDEX_ONE_SORT_KEY

    /**
     * Creates default categories for a new user.
     *
     * @param userId The identifier of the user for whom the default categories are being created.
     * @return The [List] of category identifiers created
     */
    fun createInitialCategoriesForUser(userId: String): List<InitialCreateCategory> {
        val userIdForDynamo = buildItemId(entityName = userEntityName, itemId = userId)
        val initialCategories =
            listOf(
                buildCreationMap(
                    userIdForDynamo = userIdForDynamo,
                    category =
                        CreateCategoryRequest(
                            name = "Bills",
                            colorCodeCategory = ColorCodeCategory.ONE,
                            fundId = AppUUIDGenerator.generateUUID().toString(),
                        ),
                ),
                buildCreationMap(
                    userIdForDynamo = userIdForDynamo,
                    category =
                        CreateCategoryRequest(
                            name = "Auto & Transport",
                            colorCodeCategory = ColorCodeCategory.TWO,
                            fundId = AppUUIDGenerator.generateUUID().toString(),
                        ),
                ),
                buildCreationMap(
                    userIdForDynamo = userIdForDynamo,
                    category =
                        CreateCategoryRequest(
                            name = "Out To Eat & Drinks",
                            colorCodeCategory = ColorCodeCategory.THREE,
                            fundId = AppUUIDGenerator.generateUUID().toString(),
                        ),
                ),
                buildCreationMap(
                    userIdForDynamo = userIdForDynamo,
                    category =
                        CreateCategoryRequest(
                            name = "Entertainment & Rec",
                            colorCodeCategory = ColorCodeCategory.FOUR,
                            fundId = AppUUIDGenerator.generateUUID().toString(),
                        ),
                ),
                buildCreationMap(
                    userIdForDynamo = userIdForDynamo,
                    category =
                        CreateCategoryRequest(
                            name = "Other",
                            colorCodeCategory = ColorCodeCategory.FIVE,
                            fundId = AppUUIDGenerator.generateUUID().toString(),
                        ),
                ),
            )

        val writeRequests =
            initialCategories.map { category ->
                WriteRequest.builder()
                    .putRequest(
                        PutRequest
                            .builder()
                            .item(category)
                            .build(),
                    )
                    .build()
            }

        val batchWriteItemRequest =
            BatchWriteItemRequest.builder()
                .requestItems(mapOf(tableName to writeRequests))
                .build()

        try {
            dynamoDbClient.batchWriteItem(batchWriteItemRequest)
            return initialCategories.map {
                InitialCreateCategory(
                    itemId = extractItemId(itemId = it[sortKey]?.s()),
                    description = it["description"]?.s()!!,
                    fundId = extractItemId(itemId = it[fundIdPrimaryKey]?.s()),
                )
            }
        } catch (e: Exception) {
            println("Failed to create Categories for user ${e.localizedMessage}")
            throw WheresTheFundsException("Unable to create default categories")
        }
    }

    /**
     * Creates a new category in DynamoDB based on the provided request and associates it with a user.
     *
     * @param createCategoryRequest The request containing the details for the new category.
     * @param userId The identifier of the user creating the category.
     * @return The newly created Category object.
     */
    fun createCategory(
        createCategoryRequest: CreateCategoryRequest,
        userId: String,
    ): Category {
        val userIdForDynamo = buildItemId(entityName = userEntityName, itemId = userId)
        val categoryItemId = AppUUIDGenerator.generateUUID().toString()
        val itemValues =
            buildCreationMap(
                category = createCategoryRequest,
                userIdForDynamo = userIdForDynamo,
                categoryId = categoryItemId,
            )

        return createItem(
            userId = userId,
            itemId = categoryItemId,
            itemValues = itemValues,
            entityName = categoryEntityName,
        )
    }

    /**
     * Retrieves all categories associated with a specific user.
     *
     * This method assumes category data will not exceed DynamoDB's 10MB response size limit due to the
     * expected small number and size of category objects.
     *
     * @param userId The identifier of the user whose categories are to be retrieved.
     * @return A list of Category objects belonging to the user.
     */
    fun getAllCategoriesForUser(userId: String): List<Category> {
        val userIdForDynamo = buildItemId(entityName = userEntityName, itemId = userId)
        val results =
            queryItems<Category>(
                keyConditionExpression = "#pk = :userId and begins_with(#sk, :sortKeyPrefix)",
                expressionAttributeNames =
                    mapOf(
                        "#pk" to primaryKey,
                        "#sk" to sortKey,
                    ),
                expressionAttributeValues =
                    mapOf(
                        ":userId" to AttributeValue.builder().s(userIdForDynamo).build(),
                        ":sortKeyPrefix" to AttributeValue.builder().s("$categoryEntityName#").build(),
                    ),
            )
        return results.items
    }

    /**
     * Retrieves a specific category by its identifier for a given user.
     *
     * @param userId The identifier of the user to whom the category belongs.
     * @param categoryId The unique identifier of the category to retrieve.
     * @return The requested Category object if found, or null otherwise.
     */
    fun getCategoryById(
        userId: String,
        categoryId: String,
    ) = getItemById<Category>(
        userId = userId,
        itemId = categoryId,
        entityName = categoryEntityName,
    )

    /**
     * Removes a category from the system
     *
     * @param userId The identifier of the user to whom the category belongs.
     * @param categoryId The unique identifier of the category to retrieve.
     * @return [Unit] nothing, error if failure
     */
    fun deleteCategory(
        userId: String,
        categoryId: String,
    ) = deleteItem(
        userId = userId,
        itemId = categoryId,
        entityName = categoryEntityName,
    )

    /**
     * Will populate the fund id necessary columns on the category record
     *
     * @param userId The identifier of the user to whom the category belongs.
     * @param categoryId The unique identifier of the category to retrieve.
     * @param fundId The unique identifier of the fund to retrieve.
     * @return T if successful, F otherwise
     */
    fun assignCategoryToFund(
        userId: String,
        categoryId: String,
        fundId: String,
    ): Boolean {
        val fundIdForDynamo =
            buildIndexItemId(
                entityName = categoryEntityName,
                indexEntityName = categoryEntityName,
                indexItemId = fundId,
            )
        return updateItem(
            userId = userId,
            itemId = categoryId,
            entityName = categoryEntityName,
            updateExpression = "SET #fundIdPK = :fundIdPK, #fundIdSK = :fundIdSK",
            expressionAttributeNames =
                mapOf(
                    "#fundIdPK" to fundIdPrimaryKey,
                    "#fundIdSK" to fundIdSortKey,
                ),
            expressionAttributeValues =
                mapOf(
                    ":fundIdPK" to AttributeValue.builder().s(fundIdForDynamo).build(),
                    ":fundIdSK" to AttributeValue.builder().s(fundIdForDynamo).build(),
                ),
        )
    }

    /**
     * Converts a DynamoDB item map to a [Category] object.
     *
     * This method is an implementation of the abstract method defined in DynamoService. It extracts attribute values
     * from a DynamoDB item map and constructs a [Category] object.
     *
     * @param item The map of attribute names to [AttributeValue] objects representing a category item in DynamoDB.
     * @return A [Category] object constructed from the item map, or null if required attributes are missing.
     */
    override fun toItem(item: Map<String, AttributeValue>): Category? {
        val categoryId = extractItemId(itemId = item[sortKey]?.s())
        val description = item["description"]?.s() ?: return null
        val colorCodeCategory = item["colorCodeCategory"]?.s() ?: return null
        val fundId = extractItemId(itemId = item[fundIdPrimaryKey]?.s())
        return Category(
            id = categoryId.ifEmpty { return null },
            description = description,
            colorCodeCategory = ColorCodeCategory.valueOf(colorCodeCategory),
            fundId = fundId.ifEmpty { null },
        )
    }

    /**
     * Constructs a map of attribute values for creating a new category item in DynamoDB.
     *
     * @param category The CreateCategoryRequest containing the details for the new category.
     * @param userIdForDynamo The DynamoDB item identifier for the user.
     * @param categoryId Optional specific identifier for the category; a new UUID is generated if not provided.
     * @return A map representing the DynamoDB item for the new category.
     */
    private fun buildCreationMap(
        category: CreateCategoryRequest,
        userIdForDynamo: String,
        categoryId: String? = null,
    ): Map<String, AttributeValue> {
        val categoryIdForDynamo =
            buildItemId(
                entityName = categoryEntityName,
                itemId = categoryId ?: AppUUIDGenerator.generateUUID().toString(),
            )
        return mutableMapOf(
            primaryKey to AttributeValue.builder().s(userIdForDynamo).build(),
            sortKey to AttributeValue.builder().s(categoryIdForDynamo).build(),
            "description" to AttributeValue.builder().s(category.name).build(),
            "colorCodeCategory" to AttributeValue.builder().s(category.colorCodeCategory.name).build(),
            entityTypePrimaryKey to AttributeValue.builder().s(categoryEntityName).build(),
            entityTypeSortKey to AttributeValue.builder().s(userIdForDynamo).build(),
        ).apply {
            category.fundId?.let {
                val fundIdForDynamo =
                    buildIndexItemId(
                        entityName = categoryEntityName,
                        indexEntityName = fundEntityName,
                        indexItemId = it,
                    )
                this[fundIdPrimaryKey] = AttributeValue.builder().s(fundIdForDynamo).build()
                this[fundIdSortKey] = AttributeValue.builder().s(fundIdForDynamo).build()
            }
        }
    }
}
