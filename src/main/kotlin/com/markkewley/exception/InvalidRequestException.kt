package com.markkewley.exception

/**
 * Exception that will map to a BAD_REQUEST exception.
 *
 * @param message - Some message that may be helpful to the user
 */
data class InvalidRequestException(override val message: String) : Exception(message)
