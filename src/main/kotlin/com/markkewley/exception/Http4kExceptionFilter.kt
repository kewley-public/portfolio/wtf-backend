package com.markkewley.exception

import com.markkewley.auth.exception.AuthenticationException
import org.http4k.core.Filter
import org.http4k.core.Response
import org.http4k.core.Status

/**
 * An http4k filter that handles exceptions thrown from downstream handlers.
 * This filter catches various types of exceptions and converts them into corresponding
 * HTTP responses.
 *
 * @return The filter that can be applied to an http4k server configuration.
 */
object Http4kExceptionFilter {
    val exceptionHandlingFilter =
        Filter { next ->
            { request ->
                try {
                    next(request)
                } catch (e: Exception) {
                    println("ExceptionFilter: ${e.localizedMessage}")
                    when (e) {
                        // Handles authentication-related exceptions by returning a 401 Unauthorized response.
                        is AuthenticationException -> {
                            Response(Status.UNAUTHORIZED).body(e.message)
                        }

                        // Handles invalid request exceptions by returning a 400 Bad Request response.
                        is InvalidRequestException -> {
                            Response(Status.BAD_REQUEST).body(e.message)
                        }

                        is NotFoundException -> {
                            Response(Status.NOT_FOUND).body(e.message)
                        }

                        // Handles specific business logic exceptions (e.g., insufficient funds) by returning a 500 Internal Server Error.
                        is WheresTheFundsException -> {
                            // Handle WheresTheFundsException, you can also use INTERNAL_SERVER_ERROR
                            Response(Status.INTERNAL_SERVER_ERROR).body(e.message)
                        }

                        // Handles all other exceptions by returning a generic 500 Internal Server Error.
                        else -> {
                            e.printStackTrace()
                            // Handle other exceptions
                            Response(Status.INTERNAL_SERVER_ERROR).body("Internal Server Error")
                        }
                    }
                }
            }
        }
}
