package com.markkewley.exception

/**
 * Generic exception that maps to an INTERNAL_SERVER_ERROR.
 *
 * @param message - Some message that may be helpful to the user
 */
data class WheresTheFundsException(override val message: String) : Exception(message)
