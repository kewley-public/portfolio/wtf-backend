package com.markkewley.exception

/**
 * Exception that maps to a NOT_FOUND.
 *
 * @param message - Some message that may be helpful to the user
 */
data class NotFoundException(override val message: String = "Record not found") : Exception(message)
