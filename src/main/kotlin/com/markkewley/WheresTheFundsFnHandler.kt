package com.markkewley

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.events.SQSEvent
import com.markkewley.aws.Dynamo
import com.markkewley.models.AddFundSQSMessage
import com.markkewley.serializers.CORE_JSON
import com.markkewley.services.CategoriesService
import com.markkewley.services.ExpensesService
import com.markkewley.services.FundsService
import org.http4k.serverless.FnHandler

val categoriesService = CategoriesService(dynamoDbClient = Dynamo.dynamoDbClient)
val fundsService = FundsService(dynamoDbClient = Dynamo.dynamoDbClient, categoriesService = categoriesService)
val expensesService =
    ExpensesService(
        dynamoDbClient = Dynamo.dynamoDbClient,
        categoriesService = categoriesService,
        fundsService = fundsService,
    )

val fundUpdateQueueArn = WheresTheFundsConfig.fundSqsQueueArn

val WheresTheFundsFnHandler =
    FnHandler { e: SQSEvent, _: Context ->
        println("Received $e")
        e.records.forEach {
            // Check if the event source ARN matches your queue's ARN
            if (it.eventSourceArn == fundUpdateQueueArn) {
                val event = CORE_JSON.decodeFromString<AddFundSQSMessage>(it.body)
                println("Processing event: $event")
                expensesService.setFundIdForAllRecordsByCategory(
                    userId = event.userId,
                    fundId = event.fundId,
                    categoryId = event.categoryId,
                )
                println("Finished updating records: $event")
            } else {
                println("Received an event from an unexpected source: ${it.eventSourceArn}")
            }
        }
    }
