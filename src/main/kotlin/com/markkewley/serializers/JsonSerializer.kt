package com.markkewley.serializers

import kotlinx.serialization.json.Json

// Initialize Json object with specific configuration if needed
val CORE_JSON =
    Json {
        ignoreUnknownKeys = true // Example configuration
    }
