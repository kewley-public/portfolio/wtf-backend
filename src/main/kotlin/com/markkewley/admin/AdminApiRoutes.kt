package com.markkewley.admin

import org.http4k.routing.RoutingHttpHandler
import org.http4k.routing.bind
import org.http4k.routing.routes

object AdminApiRoutes {
    fun routes(): RoutingHttpHandler =
        "/admin" bind
            routes(
                HealthCheck.routes(),
            )
}
