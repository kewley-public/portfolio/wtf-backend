package com.markkewley.admin

import org.http4k.core.Method.GET
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.routing.RoutingHttpHandler
import org.http4k.routing.bind

object HealthCheck {
    fun routes(): RoutingHttpHandler = "/ping" bind GET to { Response(Status.OK).body("pong") }
}
