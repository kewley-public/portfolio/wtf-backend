package com.markkewley.aws

import com.markkewley.WheresTheFundsConfig
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider
import software.amazon.awssdk.http.urlconnection.UrlConnectionHttpClient
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.dynamodb.DynamoDbClient
import java.net.URI

object Dynamo {
    val dynamoDbClient: DynamoDbClient

    init {
        val isLocal = WheresTheFundsConfig.runLocalDynamoDb // Boolean to determine environment
        dynamoDbClient =
            if (isLocal) {
                DynamoDbClient.builder()
                    .endpointOverride(URI.create("http://localhost:8000")) // DynamoDB Local endpoint
                    .credentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create("fakeMyKeyId", "fakeSecretAccessKey")))
                    .httpClient(UrlConnectionHttpClient.builder().build())
                    .build()
            } else {
                DynamoDbClient.builder()
                    .region(Region.of(WheresTheFundsConfig.awsRegion))
                    .build()
            }
    }
}
