import org.gradle.api.tasks.bundling.Zip
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
    repositories {
        mavenCentral()
        gradlePluginPortal()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${property("kotlinVersion")}")
        classpath("com.github.johnrengelman:shadow:8.1.1")
    }
}

plugins {
    id("com.github.johnrengelman.shadow") version "8.1.1"
    application
    kotlin("jvm") version "1.9.22"
    kotlin("plugin.serialization") version "1.9.22"
    id("org.jlleitschuh.gradle.ktlint") version "12.1.0"
}

repositories {
    mavenCentral()
}

val http4kVersion: String by project
val junitVersion: String by project
val kotlinVersion: String by project
val awsSdkVersion: String by project
val dotEnvVersion: String by project
val log4jVersion: String by project
val awsLambdaJavaEventsVersion: String by project
val javaJwtVersion: String by project
val kotlinXSerialization: String by project
val googleApiClientVersion: String by project
val jbcryptVersion: String by project
val uuidCreatorVersion: String by project
val mockkVersion: String by project

java.sourceCompatibility = JavaVersion.VERSION_17
java.targetCompatibility = JavaVersion.VERSION_17

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "17"
        freeCompilerArgs = listOf("-Xjvm-default=all")
    }
}

tasks.test {
    useJUnitPlatform()
}

dependencies {
    implementation("com.amazonaws:aws-lambda-java-events:$awsLambdaJavaEventsVersion")
    implementation("org.http4k:http4k-aws:$http4kVersion")
    implementation("org.http4k:http4k-client-okhttp:$http4kVersion")
    implementation("org.http4k:http4k-core:$http4kVersion")
    implementation("org.http4k:http4k-format-kotlinx-serialization:$http4kVersion")
    implementation("org.http4k:http4k-multipart:$http4kVersion")
    implementation("org.http4k:http4k-security-oauth:$http4kVersion")
    implementation("org.http4k:http4k-serverless-core:$http4kVersion")
    implementation("org.http4k:http4k-serverless-lambda:$http4kVersion")
    implementation("org.http4k:http4k-resilience4j:$http4kVersion")
    implementation("org.jetbrains.kotlin:kotlin-stdlib:$kotlinVersion")
    implementation("org.http4k:http4k-server-jetty:$http4kVersion")
    implementation("org.slf4j:slf4j-simple:$log4jVersion")
    implementation("io.github.cdimascio:dotenv-kotlin:$dotEnvVersion")
    implementation("com.fasterxml.jackson.core:jackson-core:2.13.0")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.13.0")
    implementation("com.auth0:java-jwt:$javaJwtVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$kotlinXSerialization")
    implementation(platform("software.amazon.awssdk:bom:$awsSdkVersion"))
    implementation("software.amazon.awssdk:http-client-spi:$awsSdkVersion")
    implementation("software.amazon.awssdk:url-connection-client:$awsSdkVersion")
    implementation("software.amazon.awssdk:apache-client:$awsSdkVersion")
    implementation("software.amazon.awssdk:dynamodb:$awsSdkVersion")
    implementation("software.amazon.awssdk:secretsmanager:$awsSdkVersion")
    implementation("com.google.api-client:google-api-client:$googleApiClientVersion")
    implementation("org.mindrot:jbcrypt:$jbcryptVersion")
    implementation("com.github.f4b6a3:uuid-creator:$uuidCreatorVersion")

    testImplementation("org.http4k:http4k-testing-approval:$http4kVersion")
    testImplementation("org.http4k:http4k-testing-chaos:$http4kVersion")
    testImplementation("org.http4k:http4k-testing-hamkrest:$http4kVersion")
    testImplementation("org.http4k:http4k-testing-kotest:$http4kVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:$junitVersion")
    testImplementation("io.mockk:mockk:$mockkVersion")
}

tasks.register<JavaExec>("runLocal") {
    mainClass.set("com.markkewley.WheresTheFundsBackend")
    classpath = sourceSets.main.get().runtimeClasspath
    standardInput = System.`in`
    group = "application"
    description = "Runs the local server for development."
}

tasks.shadowJar {
    archiveBaseName.set(project.name)
    archiveClassifier.set("")
    archiveVersion.set("")
//    manifest {
//        attributes(mapOf(
//            "Main-Class" to "com.markkewley.WheresTheFundsAppFunction"
//        ))
//    }
    mergeServiceFiles()
//    minimize()
//    dependsOn(tasks.distTar, tasks.distZip)
//    zip64.set(true)
}

tasks.register<Zip>("buildLambdaZip") {
    from(tasks.named("compileKotlin").get() as KotlinCompile)
    from(tasks.named("processResources").get())
    into("lib") {
        from(configurations.getByName("compileClasspath"))
    }
}
