import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as dynamodb from 'aws-cdk-lib/aws-dynamodb';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as apigateway from 'aws-cdk-lib/aws-apigateway';
import * as secretsmanger from 'aws-cdk-lib/aws-secretsmanager';
import * as sqs from 'aws-cdk-lib/aws-sqs';
import * as lambdaeventsources from 'aws-cdk-lib/aws-lambda-event-sources';
import * as path from 'path';
import { Duration } from 'aws-cdk-lib';

export class WheresTheFundsStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const wtfEnvironmentConfig = secretsmanger.Secret.fromSecretNameV2(this, 'WtfEnvironmentConfig', 'wtfEnvironmentConfig');

    new cdk.CfnOutput(this, 'WTFConfigArn', { value: wtfEnvironmentConfig.secretArn });
    new cdk.CfnOutput(this, 'WTFConfigName', { value: wtfEnvironmentConfig.secretName });

    // Define the DynamoDB table
    const wtfTable = new dynamodb.TableV2(this, 'WTFTable', {
      tableName: 'WTFTable',
      partitionKey: { name: 'PK', type: dynamodb.AttributeType.STRING },
      sortKey: { name: 'SK', type: dynamodb.AttributeType.STRING },
      tableClass: dynamodb.TableClass.STANDARD_INFREQUENT_ACCESS,
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    });

    wtfTable.addGlobalSecondaryIndex({
      indexName: 'GSI1',
      partitionKey: { name: 'GSI1-PK', type: dynamodb.AttributeType.STRING },
      sortKey: { name: 'GSI1-SK', type: dynamodb.AttributeType.STRING },
      projectionType: dynamodb.ProjectionType.ALL,
    });

    wtfTable.addGlobalSecondaryIndex({
      indexName: 'GSI2',
      partitionKey: { name: 'GSI2-PK', type: dynamodb.AttributeType.STRING },
      sortKey: { name: 'GSI2-SK', type: dynamodb.AttributeType.STRING },
      projectionType: dynamodb.ProjectionType.ALL,
    });

    wtfTable.addGlobalSecondaryIndex({
      indexName: 'GSI3',
      partitionKey: { name: 'GSI3-PK', type: dynamodb.AttributeType.STRING },
      sortKey: { name: 'GSI3-SK', type: dynamodb.AttributeType.STRING },
      projectionType: dynamodb.ProjectionType.ALL,
    });

    wtfTable.addGlobalSecondaryIndex({
      indexName: 'EntityType',
      partitionKey: { name: 'EntityType', type: dynamodb.AttributeType.STRING },
      projectionType: dynamodb.ProjectionType.ALL
    });

    // Output the table name
    new cdk.CfnOutput(this, 'WTFTableName', { value: wtfTable.tableName });

    const fundUpdateQueue = new sqs.Queue(this, 'FundsUpdateQueue', {
      queueName: 'FundUpdateQueue'
    });

    new cdk.CfnOutput(this, 'FundUpdateQueueArn', { value: fundUpdateQueue.queueArn });
    new cdk.CfnOutput(this, 'FundUpdateQueueUrl', { value: fundUpdateQueue.queueUrl });

    const runtime = lambda.Runtime.JAVA_17;
    const environment = {
      ENTITY_NAME_CATEGORY: 'category',
      ENTITY_NAME_EVENT: 'event',
      ENTITY_NAME_EXPENSE: 'expense',
      ENTITY_NAME_FUND: 'fund',
      ENTITY_NAME_USER: 'user',
      FUND_SQS_ARN: fundUpdateQueue.queueArn,
      FUND_SQS_URL: fundUpdateQueue.queueUrl,
      SECRET_ARN: wtfEnvironmentConfig.secretArn,
      PRIMARY_TABLE_NAME: wtfTable.tableName,
    };
    const code = lambda.Code.fromAsset(path.join(__dirname, '../../build/distributions/wtf-backend.zip'));
    const rootPacakge = 'com.markkewley';

    const proxyLambda = new lambda.Function(this, 'WTFBackendProxy', {
      functionName: 'wheres-the-funds-proxy',
      timeout: Duration.seconds(30), // API GW Timeout
      runtime,
      handler: `${rootPacakge}.WheresTheFundsAppFunction`,
      code,
      environment,
    });

    wtfEnvironmentConfig.grantRead(proxyLambda);
    wtfTable.grantReadWriteData(proxyLambda);
    fundUpdateQueue.grantSendMessages(proxyLambda);

    new cdk.CfnOutput(this, 'ProxyLambdaFunctionName', { value: proxyLambda.functionName });

    const eventLambda = new lambda.Function(this, 'WTFEventLambda', {
      functionName: 'wheres-the-funds-sqs-handler',
      runtime,
      handler: `${rootPacakge}.WheresTheFundsEventFunction`,
      code,
      environment,
    });

    wtfEnvironmentConfig.grantRead(eventLambda);
    wtfTable.grantReadWriteData(eventLambda);
    fundUpdateQueue.grantConsumeMessages(eventLambda);

    eventLambda.addEventSource(new lambdaeventsources.SqsEventSource(fundUpdateQueue, {
      batchSize: 10 // Number of messages to process at once
    }));

    new cdk.CfnOutput(this, 'EventLambdaFunctionName', { value: eventLambda.functionName });

    const apiGateway = new apigateway.LambdaRestApi(this, 'wtfapi', {
      handler: proxyLambda,
      proxy: true,
    });

    new cdk.CfnOutput(this, 'APIGatewayUrl', { value: apiGateway.url });
  }
}
