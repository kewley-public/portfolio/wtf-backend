# Welcome to your CDK TypeScript project

This is a blank project for CDK development with TypeScript.

The `cdk.json` file tells the CDK Toolkit how to execute your app.

## Useful commands

* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `npx cdk deploy`  deploy this stack to your default AWS account/region
* `npx cdk diff`    compare deployed stack with current state
* `npx cdk synth`   emits the synthesized CloudFormation template

# Dynamo Setup

## Why Dynamo Single Table?

DynamoDB's single table design has specific advantages over traditional relational databases like PostgreSQL or using
multiple tables in DynamoDB, especially in certain contexts:

Performance and Scalability: DynamoDB's single table design is optimized for performance and scalability. It minimizes
the number of read/write operations and leverages DynamoDB's fast, single-digit millisecond performance.

Cost-Effectiveness: This approach can be more cost-effective, as it reduces the overhead of managing multiple tables and
the associated read/write capacity units.

Simplified Data Retrieval: With all data in one table and using composite keys, it's often simpler to fetch related data
in a single query without the need for joins.

Reduced Latency: Since data that's often accessed together is stored closely, there can be reduced latency in data
retrieval.

However, this doesn't mean it's universally better. The choice depends on your specific use case, data access patterns,
and scalability needs. Traditional relational databases or multiple DynamoDB tables can be more suitable in scenarios
where
data is highly relational, the schema is frequently changing, or when the complexity of single table design outweighs
its performance benefits.

## Entities

Although we are using a single table design pattern it's good to list out what particular
entities may be in this table and how they relate to each other.

| Attribute  | Type   | Description                                            |
|------------|--------|--------------------------------------------------------|
| PK         | String | Primary Key for table                                  |
| SK         | String | Sort Key for table                                     |
| GSI1       | String | Global Secondary Index One                             |
| GSI2       | String | Global Secondary Index Two                             |
| GSI3       | String | Global Secondary Index Three                           |
| EntityType | String | Global Index on Entity Type Possibly use for analytics |

### User

| Attribute     | Type      | Unique |
|---------------|-----------|--------|
| id            | UUID      | Yes    |
| email         | String    | Yes    |
| userName      | String    | No     |
| passwordHash  | String    | No     |
| googleOAuthId | String    | Yes    |
| created       | Timestamp | No     |
| verified      | Boolean   | No     |
| refreshToken  | String    | Yes    |
| roles         | List      | No     |

### Category

Categories are tied to a particular User, there will be generic ones
that are created for each user. We should just create these and tie them to the
user on registration.

| Attribute | Type   | Unique |
|-----------|--------|--------|
| id        | UUID   | Yes    |
| name      | String | Yes    |
| colorCode | String | No     |

### Expense

Expenses are tied to a particular User, there are no shared expenses

| Attribute   | Type                    | Unique |
|-------------|-------------------------|--------|
| id          | ULID                    | Yes    |
| amount      | Double                  | No     |
| description | String                  | No     |
| fund        | GSI1(userId/fundId)     | Yes    |
| category    | GSI2(userId/categoryId) | Yes    |
| date        | Timestamp               | No     |

We can sort creation time based on ULID of the Expense

### Fund

Funds are tied to a particular User and a Category, there are no shared funds

| Attribute   | Type                    | Unique |
|-------------|-------------------------|--------|
| id          | UID                     | Yes    |
| description | String                  | No     |
| category    | GSI1(userId,categoryId) | No     |
| amount      | Double                  | No     |

### Event

Events are tied to a particular User and a Category, there are no shared events

| Attribute   | Type                    | Unique |
|-------------|-------------------------|--------|
| id          | ULID                    | Yes    |
| description | String                  | No     |
| amount      | Double                  | No     |
| date        | Timestamp               | No     |
| category    | GSI1(userId,categoryId) | No     |

## Write Patterns

| Pattern                  | Table/GSI | Key Condition           | Attributes                                |
|--------------------------|-----------|-------------------------|-------------------------------------------|
| Create a User            | User      | PK=userId,SK=userId     | (id, email, username, passwordHash)       |
| Create a User by OAuthId | GSI1      | PK=email,SK=email       | (id, email, username, <oAuthIdKey>)       |
| Create a Fund            | Fund      | PK=userId,SK=fundId     | (id, description, category, amount)       |
| Create an Expense        | Expense   | PK=userId,SK=expenseId  | (id, amount, description, category, date) |
| Update an Expense        | Expense   | PK=userId,SK=expenseId  | (amount, description, category, date)     |
| Remove an Expense        | Expense   | PK=userId,SK=expenseId  |                                           |
| Create a Category        | Category  | PK=userId,SK=categoryId | (id, name)                                |
| Remove a Category        | Category  | PK=userId,SK=categoryId |                                           |
| Create an Event          | Event     | PK=userId,SK=eventId    | (id, description, amount, date, category) |
| Update an Event          | Event     | PK=userId,SK=eventId    | (description, amount, date, category)     |
| Remove an Event          | Event     | PK=userId,SK=eventId    |                                           |

## Access Patterns

| Pattern                                           | Table/GSI | Key Condition           | Example                                                  |
|---------------------------------------------------|-----------|-------------------------|----------------------------------------------------------|
| Get User by Email                                 | GSI1      | PK=email,SK=email       | PK="user#test@test.com" and SK="user#test@test.com"      |
| Get User by Refresh Token                         | GSI2      | PK=token,SK=token       | PK="token" and SK="token"                                |
| Get User by Verification Token                    | GSI3      | PK=token,SK=token       | PK="token" and SK="token"                                |
| Get Expenses for User                             | Table     | PK=userId,SK=expenseId  | PK="user#12345" and SK begins_with "expense#"            |
| Get Funds for User                                | Table     | PK=userId,SK=fundId     | PK="user#12345" and SK begins_with "fund#"               |
| Get Events for User                               | Table     | PK=userId,SK=eventId    | PK="user#12345" and SK begins_with "event#"              |
| Get Categories for User                           | Table     | PK=userId,SK=categoryId | PK="user#1245" and SK begins_with "category#"            |
| Get Expenses By Funds for a given date range      | GSI1      | PK=userId,SK=fundId     | PK="user#12345" and SK between "ISO-8061" and "ISO-8061" |
| Get Expenses By Categories for a given date range | GSI2      | PK=userId,SK=categoryId | PK="user#12345" and SK between "ISO-8061" and "ISO-8061" |
| Get Events By User for a given date range         | GSI1      | pk=userId,SK=dates      | PK="user#12345" and SK between "ISO-8061" and "ISO-8061" |
| Get Fund by Category                              | GSI1      | pk=                     |                                                          |
