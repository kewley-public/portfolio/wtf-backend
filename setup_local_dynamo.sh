#!/bin/bash

docker compose up -d

# Endpoint for local DynamoDB
ENDPOINT_URL="http://localhost:8000"

# Function to check if a DynamoDB table exists
check_table_exists() {
    aws dynamodb describe-table --table-name $1 --endpoint-url $ENDPOINT_URL > /dev/null 2>&1
    return $?
}

# Create WTFTable table if it doesn't exist
if check_table_exists "WTFTable"; then
    echo "Table 'WTFTable' already exists in local DynamoDB."
else
    echo "Creating table 'WTFTable' in local DynamoDB."
    aws dynamodb create-table --cli-input-json file://localdynamo/wheresTheFundsTable.json --endpoint-url $ENDPOINT_URL > /dev/null
fi